module.exports = (api) => {
    api.cache(true);

    const isCJS = process.env.BABEL_ENV === 'cjs';
    const isTest = process.env.NODE_ENV === 'test';

    const defaultPresets = [
        ['@babel/preset-env', { modules: isCJS || isTest ? 'commonjs' : false }],
        '@babel/preset-react',
        '@babel/preset-typescript'
    ];

    const defaultPlugins = [
        ['emotion', { sourceMap: true }],
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-proposal-export-default-from',
        // Because Webpack 4 does not support the Optional Chaining
        // Operator, the corresponding plugin is added here manually.
        // This way we stay on the safe side in this respect, even
        // if the .browserlistrc file would only target new browsers.
        '@babel/plugin-proposal-optional-chaining',
        ['@babel/plugin-transform-runtime', { version: '^7.5.0', corejs: 3 }]
    ];

    const productionPlugins = [
        '@babel/plugin-transform-react-jsx',
        '@babel/plugin-transform-react-constant-elements'
    ];

    return {
        presets: defaultPresets,
        plugins: defaultPlugins,
        retainLines: false,
        comments: false,
        ignore: [
            /@babel[\\|/]runtime/,
            '**/__mocks__'
        ],
        env: {
            esm: {
                plugins: productionPlugins,
                ignore: [
                    /\.test\.ts/
                ]
            },
            cjs: {
                plugins: productionPlugins,
                ignore: [
                    /\.test\.ts/
                ]
            }
        }
    };
};
