const { resolve } = require('path');

module.exports = {
    root: true,
    extends: [
        '@heinzelman-labs/eslint-config-react',
        '@heinzelman-labs/eslint-config-typescript'
    ],
    rules: {
        'import/no-extraneous-dependencies': ['error', {
            packageDir: [
                __dirname,
                resolve(__dirname, 'packages/bleep-core'),
                resolve(__dirname, 'packages/bleep-demo'),
                resolve(__dirname, 'packages/bleep-react')
            ]
        }],
        // todo update `@heinzelman-labs/eslint-config-typescript`
        '@typescript-eslint/no-empty-function': ['error', {
            allow: ['arrowFunctions']
        }],
        // todo update `@heinzelman-labs/eslint-config-typescript`
        '@typescript-eslint/no-explicit-any': 'off'
    },
    globals: {
        jest: 'readonly'
    },
    env: {
        browser: true,
        node: true,
        commonjs: true,
        es6: true
    }
};
