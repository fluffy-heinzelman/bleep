# MIT License

Copyright (c) 2020-present Micha Meyer <heinzelman.code@gmail.com>

Portions of this software are licensed as follows:

* All content located in the "packages/bleep-demo/src/assets/" directory of this repository is subject to the licenses of the respective authors.
    * [Background Sounds](./packages/bleep-demo/ASSETS.md#background-sounds)
    * [Effect Sounds](./packages/bleep-demo/ASSETS.md#effect-sounds)
    * [Foreground Sounds](./packages/bleep-demo/ASSETS.md#foreground-sounds)
* Content outside of the above mentioned directory is available under the "MIT Expat" license as defined below.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
