# Bleep

*This is still work-in-progress!*

`Bleep` is a generic audio manager made with web games in mind.

## Demo

Have fun playing with the [Demo](https://fluffy-heinzelman.gitlab.io/bleep/).

## Packages

### `@heinzelman-labs/bleep-core`

`Bleep` core classes and utilities.

[Source](./packages/bleep-core)

### `@heinzelman-labs/bleep-react`

Everything you need to use `Bleep` in React based projects.

[Source](./packages/bleep-react)

## Credits

The development of Bleep would have been half the fun without the hard work of some great music artists and game enthusiasts. Special thanks therefore go to, among others:

* [FesliyanStudios](https://www.fesliyanstudios.com)
* [sawsquarenoise](http://rolemusic.sawsquarenoise.com/p/sawsquarenoise.html)
* [OpenGameArt](https://opengameart.org/)

## License

All content located in the "packages/bleep-demo/src/assets/" directory of this repository is subject to the licenses of the respective authors.

* [Background Sounds](./packages/bleep-demo/ASSETS.md#background-sounds)
* [Effect Sounds](./packages/bleep-demo/ASSETS.md#effect-sounds)
* [Foreground Sounds](./packages/bleep-demo/ASSETS.md#foreground-sounds)

Bleep software is [MIT licensed](./LICENSE.md).
