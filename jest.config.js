const lernaAliases = require('lerna-alias');

module.exports = {
    collectCoverage: true,
    collectCoverageFrom: [
        'packages/**/*.{js,jsx,ts,tsx}',
        '!**/node_modules/**',
        '!**/__tests__/**'
    ],
    coverageDirectory: 'coverage',
    coveragePathIgnorePatterns: [
        '[\\/]+build[\\/]+',
        '[\\/]+node_modules[\\/]+'
    ],
    coverageReporters: [
        'json',
        'text',
        'lcov'
    ],
    moduleDirectories: [
        'node_modules',
        'jest',
        __dirname
    ],
    moduleFileExtensions: [
        'js',
        'ts',
        'tsx'
    ],
    moduleNameMapper: lernaAliases.jest(),
    modulePathIgnorePatterns: [
        '[\\/]+build[\\/]+',
        '[\\/]+node_modules[\\/]+'
    ],
    projects: [
        '<rootDir>/packages/*/jest.config.js'
    ],
    rootDir: __dirname,
    roots: [
        '<rootDir>/packages'
    ],
    setupFilesAfterEnv: [
        '<rootDir>/jest/setupTests.js'
    ],
    snapshotSerializers: ['jest-emotion'],
    testEnvironment: 'jsdom',
    testMatch: [
        '**/__tests__/**/*.[jt]s?(x)',
        '**/?(*.)+(spec|test).[tj]s?(x)'
    ],
    testPathIgnorePatterns: [
        '[\\/]+build[\\/]+',
        '[\\/]+node_modules[\\/]+'
    ],
    transform: {
        '^.+\\.tsx?$': 'ts-jest',
        '^.+\\.jsx?$': '<rootDir>/jest/jsxTransform.js',
        '^(?!.*\\.(js|jsx|ts|tsx|css|json)$)': '<rootDir>/jest/fileTransform.js'
    },
    transformIgnorePatterns: [
        '[\\/]+node_modules[\\/]+(?!lodash-es)'
    ],
    globals: {
        'ts-jest': {
            diagnostics: false
        }
    }
};
