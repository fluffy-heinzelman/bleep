export const columns = 12;
export const gutter = '0.25rem';

export const lineHeight = 1.5;
export const borderRadius = '0.125rem';

export const palettes = {
    black: '#000',
    white: '#FFF',
    primary: {
        lighter: '#c9a6fb',
        light: '#b685fa',
        main: '#a265f8',
        dark: '#8e44f7',
        darker: '#7b24f5',
        contrast: {
            lighter: '#000',
            light: '#000',
            main: '#FFF',
            dark: '#FFF',
            darker: '#FFF'
        }
    },
    neutral: {
        lighter: '#737373',
        light: '#5a5a5a',
        main: '#494949',
        dark: '#272727',
        darker: '#101010',
        contrast: {
            lighter: '#FFF',
            light: '#FFF',
            main: '#FFF',
            dark: '#FFF',
            darker: '#FFF'
        }
    }
};

export const focusSize = '0.125rem';
export const focusColor = palettes.primary.contrast.main;
export const focusShadow = `0 0 0 ${focusSize} ${focusColor}`;
