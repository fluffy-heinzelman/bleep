/** @jsx jsx */
import { jsx, css, Global } from '@emotion/core';
import React, { FunctionComponent, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { SanitizeCSS } from 'emotion-sanitize';
import { Master } from '@heinzelman-labs/bleep-react';

import { App } from './components';
import { palettes } from './theme';

// Add fonts
import '@fontsource/roboto/latin-400.css';

const globalStyle = css`
    html, body, #app {
        font-family: "Roboto";
        width: 100%;
        color: ${palettes.neutral.lighter};
        background: ${palettes.neutral.darker};
    }
`;

const Base: FunctionComponent = ({ children }) => {
    return (
        <Fragment>
            <Global styles={globalStyle} />
            <SanitizeCSS sanitize typography forms />
            <Master id="master" volume={1}>
                {children}
            </Master>
        </Fragment>
    );
};

Base.displayName = 'Base';

ReactDOM.render(<Base><App /></Base>, document.getElementById('app'));
