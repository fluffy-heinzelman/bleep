# Asset Creators & Copyright

Please see [`ASSETS.md`](../../../ASSETS.md#background-sounds).

# Notes

All files normalized to 85.0 dB.
