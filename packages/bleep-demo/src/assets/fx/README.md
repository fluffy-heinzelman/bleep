# Asset Creators & Copyright

Please see [`ASSETS.md`](../../../ASSETS.md#effect-sounds).

# Notes

All files normalized to 86.0 dB.
