# Asset Creators & Copyright

Please see [`ASSETS.md`](../../../ASSETS.md#foreground-sounds).

# Notes

All files normalized to 89.0 dB.
