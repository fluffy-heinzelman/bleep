import { Loader, Resource, XhrLoadStrategy, XhrResponseType } from 'resource-loader';
import { webAudioContext } from '@heinzelman-labs/bleep-core';

const baseURL = '';
const concurrency = 5;

XhrLoadStrategy.setExtensionXhrType('mp3', XhrResponseType.Buffer);
Resource.setLoadStrategy('mp3', XhrLoadStrategy);

function decodeAudio(resource: Resource, next: () => void): void {
    if (!webAudioContext || !resource.url.endsWith('mp3')) {
        next();
        return;
    }

    webAudioContext.decodeAudioData(resource.data)
        /* eslint-disable-next-line no-param-reassign */
        .then((buffer) => { resource.data = buffer; })
        /* eslint-disable-next-line no-console */
        .catch(console.error)
        .finally(next);
}

Loader.use(decodeAudio);

const loader = new Loader(baseURL, concurrency);

export { loader };
