/** @jsx jsx */
import { jsx } from '@emotion/core';
import { cloneElement, isValidElement, ReactNode } from 'react';

/**
 * Easily clone a child element to apply a new `css` prop to it.
 *
 * @param {ReactNode} child
 * @param {Object.<string, any>} [props = {}]
 * @return {ReactNode}
 */
export function cloneChild(
    child: ReactNode,
    props: { [key: string]: unknown } = {}
): ReactNode {
    if (!isValidElement(child)) return child;

    const p = { ...props, ...child.props };

    // If the element to be cloned is a react component with
    // css already applied to it, use React.cloneElement
    // because `jsx` fn wouldn't accept the element type.
    if (Object.prototype.hasOwnProperty.call(child.props, 'css')) {
        return cloneElement(child, {
            ...p,
            css: [props.css, child.props.css]
        });
    }

    return jsx(child.type, { key: child.key, ...p });
}
