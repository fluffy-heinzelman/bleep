/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { useMemo } from 'react';
import { Audio } from '@heinzelman-labs/bleep-core';

import {
    ButtonGroup,
    Duration,
    Fade,
    Mute,
    Loop,
    Position,
    Resume,
    Start,
    Stop,
    Suspend,
    Volume
} from '..';

const createStyle = (): {
    root: SerializedStyles[];
    wrap: SerializedStyles[];
    noWrap: SerializedStyles[];
    groupGap: SerializedStyles[];
} => {
    const root = css`
        & > td {
            padding: 0.375rem;

            &:first-of-type {
                padding-left: 0;
                word-break: normal;
            }

            &:last-of-type {
                padding-right: 0;
                text-align: right;
            }
        }
    `;

    const wrap = css`
        white-space: normal;
    `;

    const noWrap = css`
        white-space: nowrap;
    `;

    const groupGap = css`
        margin-right: 0.75rem;
    `;

    return {
        root: [root],
        wrap: [wrap],
        noWrap: [noWrap],
        groupGap: [groupGap]
    };
};

export interface AudioItemProps {
    target: Audio;
}

const AudioItem = ({ target }: AudioItemProps): JSX.Element | null => {
    const style = useMemo(() => createStyle(), []);

    return !target ? null : (
        <tr css={style.root}>
            <td css={style.wrap}>
                {target?.id}
            </td>
            <td css={style.wrap}>
                <span css={style.noWrap}>
                    <Position target={target} />
                    &nbsp;/&nbsp;
                </span>
                <span css={style.noWrap}>
                    <Duration css={style.noWrap} target={target} />
                </span>
            </td>
            <td>
                <Loop target={target} />
            </td>
            <td>
                <Mute target={target} />
            </td>
            <td>
                <Volume target={target} />
            </td>
            <td>
                <ButtonGroup
                    role="toolbar"
                    aria-label="Toolbar"
                    wrap={false}
                >
                    <ButtonGroup
                        css={style.groupGap}
                        aria-label="Start/stop channel"
                        wrap={false}
                    >
                        <Start target={target} size="S" />
                        <Stop target={target} size="S" />
                    </ButtonGroup>
                    <ButtonGroup
                        css={style.groupGap}
                        aria-label="Suspend/resume channel"
                        wrap={false}
                    >
                        <Suspend target={target} size="S" />
                        <Resume target={target} size="S" />
                    </ButtonGroup>
                    <ButtonGroup
                        aria-label="Fade channel in/out"
                        wrap={false}
                    >
                        <Fade target={target} size="S" />
                    </ButtonGroup>
                </ButtonGroup>
            </td>
        </tr>
    );
};

AudioItem.displayName = 'AudioItem';

export { AudioItem };
