/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { ChangeEvent, useCallback, useMemo, useState } from 'react';
import { EventTarget, MuteStateListener } from '@heinzelman-labs/bleep-core';
import { useEvent } from '@heinzelman-labs/bleep-react';

import { Checkbox, CheckboxProps } from '../Checkbox';

const createStyle = (): {
    root: SerializedStyles[];
} => {
    const root = css`

    `;

    return {
        root: [root]
    };
};

export interface MuteProps extends Omit<CheckboxProps, 'checked'> {
    target?: EventTarget;
}

const Mute = ({
    target,
    ...other
}: MuteProps): JSX.Element | null => {
    const style = useMemo(() => createStyle(), []);
    const [muted, setMuted] = useState<boolean>(Boolean(target?.muted));
    const { onChange } = other;

    const changeHandler = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if (onChange) onChange(event);
        /* eslint-disable-next-line no-param-reassign */
        if (target) target.muted = event.target.checked;
    }, [onChange, target]);

    const muteStateHandler = useCallback<MuteStateListener>(({ muted }) => {
        setMuted(muted);
    }, []);

    useEvent(target, 'mute-state', muteStateHandler);

    return !target ? null : (
        <Checkbox
            css={style.root}
            name={`mute-${target.id}`}
            {...other}
            checked={muted}
            onChange={changeHandler}
        />
    );
};

Mute.displayName = 'Mute';

export { Mute };
