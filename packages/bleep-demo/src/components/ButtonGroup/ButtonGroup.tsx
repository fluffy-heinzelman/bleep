/** @jsx jsx */
import { jsx, css, SerializedStyles } from '@emotion/core';
import React, { HTMLAttributes, FunctionComponent, isValidElement, useMemo } from 'react';

import { cloneChild } from '../../helpers';

const noLeftBorder = css({
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0
});

const noRightBorder = css({
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0
});

const createStyle = (
    props: Required<Pick<ButtonGroupProps, 'wrap'>>
): {
    root: SerializedStyles[];
} => {
    const { wrap } = props;

    const root = css`
        display: inline-flex;
        flex-wrap: ${!wrap ? 'nowrap' : 'wrap'};
        vertical-align: middle;
    `;

    return {
        root: [root]
    };
};

export interface ButtonGroupProps extends HTMLAttributes<HTMLDivElement> {
    /**
     * If children should wrap onto multiple lines when the parent
     * content box is not wide enough.
     * @default true
     */
    wrap?: boolean;

    /**
     * Disable all button children.
     */
    disabled?: boolean;

    /**
     * To support assistive technologies (such as screen readers),
     * an appropriate role attribute needs to be provided. For a
     * single button group, this would be `role="group"`, while
     * toolbars (nested groups) should have a `role="toolbar"`.
     * @default group
     */
    role?: string;

    /**
     * In addition to `role`, groups and toolbars should be given
     * an explicit label, as most assistive technologies will
     * otherwise not announce them, despite the presence of the
     * correct `role` attribute.
     * @default undefined
     */
    'aria-label'?: string;
}

const ButtonGroup: FunctionComponent<ButtonGroupProps> = ({
    wrap = true,
    disabled,
    role = 'group',
    children,
    ...other
}: ButtonGroupProps) => {
    const style = useMemo(() => createStyle({ wrap }), [wrap]);
    const totalChildren = React.Children.count(children);
    const buttons = useMemo(() => React.Children.map(children, (child, index) => {
        if (!isValidElement) return child;

        const style: SerializedStyles[] = [];
        index !== 0 && style.push(noLeftBorder);
        index !== totalChildren - 1 && style.push(noRightBorder);

        const props: { [key: string]: any } = { css: style };
        if (disabled) props.disabled = true;

        return cloneChild(child, props);
    }), [children, totalChildren, disabled]);

    return (
        <div
            css={style.root}
            role={role}
            {...other}
        >
            {buttons}
        </div>
    );
};

ButtonGroup.displayName = 'ButtonGroup';

export { ButtonGroup };
