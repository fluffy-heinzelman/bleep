/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { HTMLAttributes, useMemo } from 'react';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { palettes } from '../../theme';

const createStyle = (): {
    root: SerializedStyles[];
    icon: SerializedStyles[];
} => {
    const root = css`
        position: absolute;
        z-index: 999;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        font-size: 60px;
    `;

    const icon = css`
        position: absolute;
        top: 50%;
        left: 50%;
        margin-top: -30px;
        margin-left: -30px;
        color: ${palettes.primary.main};
    `;

    return {
        root: [root],
        icon: [icon]
    };
};

export type SpinnerProps = HTMLAttributes<HTMLDivElement>;

const Spinner = ({
    ...other
}: SpinnerProps): JSX.Element | null => {
    const style = useMemo(() => createStyle(), []);
    return (
        <div
            css={style.root}
            {...other}
        >
            <FontAwesomeIcon
                css={style.icon}
                icon={faSpinner}
                spin
            />
        </div>
    );
};

Spinner.displayName = 'Spinner';

export { Spinner };
