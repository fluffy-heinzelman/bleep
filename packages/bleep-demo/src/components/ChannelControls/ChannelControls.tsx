/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { HTMLAttributes, useMemo } from 'react';
import { Channel } from '@heinzelman-labs/bleep-core';
import { useChannel } from '@heinzelman-labs/bleep-react';

import { borderRadius, palettes } from '../../theme';

import {
    AudioList,
    ButtonGroup,
    Fade,
    Label,
    Mute,
    Resume,
    Suspend,
    Volume
} from '..';

const createStyle = (): {
    root: SerializedStyles[];
    flexContainer: SerializedStyles[];
    mute: SerializedStyles[];
    volume: SerializedStyles[];
    groupGap: SerializedStyles[];
} => {
    const root = css`
        position: relative;
        padding: 1rem;
        margin-bottom: 0.5rem;
        border-radius: ${borderRadius};
        background: ${palettes.neutral.dark};
        user-select: none;

        > h2 {
            margin-top: 0;
        }
    `;

    const flexContainer = css`
        display: flex;
    `;

    const mute = css`
        flex-shrink: 1;
        margin-right: 1rem;
    `;

    const volume = css`
        flex-grow: 1;

        > div {
            width: 100%;
        }
    `;

    const groupGap = css`
        margin-right: 0.75rem;
    `;

    return {
        root: [root],
        flexContainer: [flexContainer],
        mute: [mute],
        volume: [volume],
        groupGap: [groupGap]
    };
};

export interface ChannelControlsProps extends HTMLAttributes<HTMLDivElement> {
    /**
     * Channel title.
     */
    title: string;

    /**
     * Channel instance or id.
     */
    channel: string | Channel;
}

const ChannelControls = ({
    title,
    channel: channelProp,
    ...other
}: ChannelControlsProps): JSX.Element => {
    const style = useMemo(() => createStyle(), []);
    const [channel] = useChannel(channelProp);

    return (
        <div css={style.root} {...other}>
            <h2>{title}</h2>

            <ButtonGroup
                css={css`position: absolute; top: 1rem; right: 1rem`}
                role="toolbar"
                aria-label="Toolbar"
            >
                <ButtonGroup
                    css={style.groupGap}
                    aria-label="Suspend/resume channel"
                    wrap={false}
                >
                    <Suspend target={channel} />
                    <Resume target={channel} />
                </ButtonGroup>
                <ButtonGroup
                    aria-label="Fade channel in/out"
                    wrap={false}
                >
                    <Fade target={channel} />
                </ButtonGroup>
            </ButtonGroup>

            <div css={style.flexContainer}>
                <Label
                    css={style.mute}
                    label="Mute"
                    position="top"
                >
                    <Mute target={channel} />
                </Label>
                <Label
                    css={style.volume}
                    label="Volume"
                    position="top"
                >
                    <Volume target={channel} />
                </Label>
            </div>

            <AudioList target={channel} />
        </div>
    );
};

ChannelControls.displayName = 'ChannelControls';

export { ChannelControls };
