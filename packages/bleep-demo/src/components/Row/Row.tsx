/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { HTMLAttributes, ReactNode, useMemo } from 'react';

import { gutter } from '../../theme';

const createStyle = (): {
    root: SerializedStyles[];
} => {
    const root = css`
        display: flex;
        flex-wrap: wrap;
        margin: 0 -${gutter};
    `;

    return {
        root: [root]
    };
};

export interface RowProps extends HTMLAttributes<HTMLDivElement> {
    /** @ignore */
    children?: ReactNode;
}

const Row = ({
    children,
    ...other
}: RowProps): JSX.Element => {
    const style = useMemo(() => createStyle(), []);

    return (
        <div css={style.root} {...other}>
            {children}
        </div>
    );
};

Row.displayName = 'Row';

export { Row };
