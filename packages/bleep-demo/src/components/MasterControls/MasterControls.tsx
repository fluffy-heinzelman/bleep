/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { HTMLAttributes, useMemo } from 'react';
import { Master } from '@heinzelman-labs/bleep-core';
import { useMaster } from '@heinzelman-labs/bleep-react';

import { borderRadius, palettes } from '../../theme';

import {
    ButtonGroup,
    Fade,
    Label,
    Mute,
    Resume,
    Suspend,
    Volume
} from '..';

const createStyle = (): {
    root: SerializedStyles[];
    flexContainer: SerializedStyles[];
    mute: SerializedStyles[];
    volume: SerializedStyles[];
    groupGap: SerializedStyles[];
} => {
    const root = css`
        position: relative;
        padding: 1rem;
        margin-bottom: 0.5rem;
        border-radius: ${borderRadius};
        background: ${palettes.neutral.dark};
        user-select: none;

        > h1 {
            margin-top: 0;
        }
    `;

    const flexContainer = css`
        display: flex;
    `;

    const mute = css`
        flex-shrink: 1;
        margin-right: 1rem;
    `;

    const volume = css`
        flex-grow: 1;

        > div {
            width: 100%;
        }
    `;

    const groupGap = css`
        margin-right: 0.75rem;
    `;

    return {
        root: [root],
        flexContainer: [flexContainer],
        mute: [mute],
        volume: [volume],
        groupGap: [groupGap]
    };
};

export interface MasterControlsProps extends HTMLAttributes<HTMLDivElement> {
    /**
     * Master title.
     * @default Master
     */
    title?: string;

    /**
     * Master instance or id.
     */
    master?: string | Master;
}

const MasterControls = ({
    title = 'Master',
    master: masterProp,
    ...other
}: MasterControlsProps): JSX.Element => {
    const style = useMemo(() => createStyle(), []);
    const master = useMaster(masterProp);

    return (
        <div css={style.root} {...other}>
            <h1>{title}</h1>

            <ButtonGroup
                css={css`position: absolute; top: 1rem; right: 1rem`}
                role="toolbar"
                aria-label="Toolbar"
            >
                <ButtonGroup
                    css={style.groupGap}
                    aria-label="Suspend/resume channel"
                    wrap={false}
                >
                    <Suspend target={master} />
                    <Resume target={master} />
                </ButtonGroup>
                <ButtonGroup
                    aria-label="Fade channel in/out"
                    wrap={false}
                >
                    <Fade target={master} />
                </ButtonGroup>
            </ButtonGroup>

            <div css={style.flexContainer}>
                <Label
                    css={style.mute}
                    label="Mute"
                    position="top"
                >
                    <Mute target={master} />
                </Label>
                <Label
                    css={style.volume}
                    label="Volume"
                    position="top"
                >
                    <Volume target={master} />
                </Label>
            </div>
        </div>
    );
};

MasterControls.displayName = 'MasterControls';
export { MasterControls };
