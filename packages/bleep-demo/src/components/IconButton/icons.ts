import { FontAwesomeIconProps } from '@fortawesome/react-fontawesome';
import {
    faPlay,
    faPause,
    faRedoAlt,
    faSortAmountDown,
    faSortAmountUp,
    faStop
} from '@fortawesome/free-solid-svg-icons';

export type Icon = 'start' | 'stop' | 'suspend' | 'resume' | 'fadeOut' | 'fadeIn';
export type Icons = {
    [key in Icon]: FontAwesomeIconProps;
};

export const icons: Icons = {
    start: {
        icon: faPlay
    },
    stop: {
        icon: faStop
    },
    suspend: {
        icon: faPause
    },
    resume: {
        icon: faRedoAlt,
        flip: 'vertical'
    },
    fadeOut: {
        icon: faSortAmountDown
    },
    fadeIn: {
        icon: faSortAmountUp
    }
};
