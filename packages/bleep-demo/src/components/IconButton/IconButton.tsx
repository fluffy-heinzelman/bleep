/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { ButtonHTMLAttributes, ReactNode, useMemo } from 'react';
import { FontAwesomeIcon, FontAwesomeIconProps } from '@fortawesome/react-fontawesome';

import { borderRadius, focusShadow, palettes } from '../../theme';

const createStyle = (
    props: Required<Pick<IconButtonProps, 'disabled' | 'size'>>
): {
    root: SerializedStyles[];
} => {
    const { disabled: disabledProp, size: sizeProp } = props;

    const root = css`
        vertical-align: middle;
        background: ${palettes.primary.main};
        color: ${palettes.primary.contrast.main};
        border: 0;
        border-radius: ${borderRadius};
        appearance: none;
        cursor: pointer;
        user-select: none;
        overflow: hidden;

        & * {
            pointer-events: none;
            user-select: none;
        }

        &:hover:not(:disabled) {
            background-color: ${palettes.primary.dark};
            color: ${palettes.primary.contrast.dark};
        }

        &:active:not(:disabled) {
            background-color: ${palettes.primary.darker};
            color: ${palettes.primary.contrast.darker};
        }

        &:focus:not(:disabled):not(:active):not(:hover) {
            background-color: ${palettes.primary.dark};
            color: ${palettes.primary.contrast.dark};
            outline: 0;
            box-shadow: ${focusShadow};
        }
    `;

    const size = sizeProp === 'S' ? css`
        width: 1.75rem;
        height: 1.75rem;
        font-size: 0.75rem;
    ` : css`
        width: 2rem;
        height: 2rem;
        font-size: 1rem;
    `;

    const disabled = !disabledProp ? css`` : css`
        background-color: ${palettes.neutral.lighter};
        color: ${palettes.neutral.contrast.lighter};
        opacity: 0.7;
        cursor: inherit;
        pointer-events: none;
    `;

    return {
        root: [root, size, disabled]
    };
};

export type IconButtonSize = 'S' | 'M';

export interface IconButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    /**
     * Icon button size, either `S` or `M`.
     * @default M
     */
    size?: IconButtonSize;

    /**
     * Icon component props.
     */
    iconProps: FontAwesomeIconProps;

    /** @ignore */
    children?: ReactNode;
}

const IconButton = ({
    size = 'M',
    disabled = false,
    iconProps,
    children,
    ...other
}: IconButtonProps): JSX.Element => {
    const style = useMemo(() => createStyle({ disabled, size }), [disabled, size]);

    return (
        <button
            css={style.root}
            type="button"
            disabled={disabled}
            {...other}
        >
            <FontAwesomeIcon
                role="presentation"
                aria-hidden
                focusable={false}
                {...iconProps}
            />
            {children}
        </button>
    );
};

IconButton.displayName = 'IconButton';

export { IconButton };
