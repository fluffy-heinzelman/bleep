import React, { ReactElement, useCallback, useState } from 'react';
import { Audio, PositionListener } from '@heinzelman-labs/bleep-core';
import { useEvent } from '@heinzelman-labs/bleep-react';

export interface PositionProps {
    target?: Audio;
}

const Position = ({ target }: PositionProps): ReactElement => {
    const [position, setPosition] = useState<number>(target?.position || 0);

    const positionHandler = useCallback<PositionListener>(({ position }) => {
        setPosition(position);
    }, []);

    useEvent(target, 'position', positionHandler);

    return <>{new Date(position * 1000).toISOString().substr(14, 5)}</>;
};

export { Position };
