/* eslint-disable @typescript-eslint/no-non-null-assertion */
/** @jsx jsx */
import { jsx, css, SerializedStyles } from '@emotion/core';
import React, { FunctionComponent, useEffect, useState, useMemo } from 'react';
import { Audio, Channel } from '@heinzelman-labs/bleep-react';

import { loader } from '../../helpers';
import { ChannelControls } from '../ChannelControls';
import { Column } from '../Column';
import { MasterControls } from '../MasterControls';
import { Row } from '../Row';
import { Spinner } from '../Spinner';

import bg1 from '../../assets/bg/bg1.mp3';
import bg2 from '../../assets/bg/bg2.mp3';
import bg3 from '../../assets/bg/bg3.mp3';
import bg4 from '../../assets/bg/bg4.mp3';
import bg5 from '../../assets/bg/bg5.mp3';

import fx1 from '../../assets/fx/fx1.mp3';
import fx2 from '../../assets/fx/fx2.mp3';
import fx3 from '../../assets/fx/fx3.mp3';
import fx4 from '../../assets/fx/fx4.mp3';
import fx5 from '../../assets/fx/fx5.mp3';
import fx6 from '../../assets/fx/fx6.mp3';
import fx7 from '../../assets/fx/fx7.mp3';
import fx8 from '../../assets/fx/fx8.mp3';
import fx9 from '../../assets/fx/fx9.mp3';

import fg1 from '../../assets/fg/fg1.mp3';
import fg2 from '../../assets/fg/fg2.mp3';
import fg3 from '../../assets/fg/fg3.mp3';
import fg4 from '../../assets/fg/fg4.mp3';
import fg5 from '../../assets/fg/fg5.mp3';

const createStyle = (): {
    root: SerializedStyles[];
} => {
    const root = css`
        width: 100%;
        margin: 0 auto;
        padding: 0.5rem;
    `;

    return {
        root: [root]
    };
};

const App: FunctionComponent = () => {
    const style = useMemo(() => createStyle(), []);
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        loader
            .add('bg1', bg1)
            .add('bg2', bg2)
            .add('bg3', bg3)
            .add('bg4', bg4)
            .add('bg5', bg5)
            .add('fx1', fx1)
            .add('fx2', fx2)
            .add('fx3', fx3)
            .add('fx4', fx4)
            .add('fx5', fx5)
            .add('fx6', fx6)
            .add('fx7', fx7)
            .add('fx8', fx8)
            .add('fx9', fx9)
            .add('fg1', fg1)
            .add('fg2', fg2)
            .add('fg3', fg3)
            .add('fg4', fg4)
            .add('fg5', fg5)
            .load(() => {
                setLoaded(true);
            });
    }, []);

    return !loaded ? <Spinner /> : (
        <div css={style.root}>
            <Channel id="bg" volume={0.5}>
                <Audio id="bg1" src={loader.resources.bg1!.data as AudioBuffer} />
                <Audio id="bg2" src={loader.resources.bg2!.data as AudioBuffer} />
                <Audio id="bg3" src={loader.resources.bg3!.data as AudioBuffer} />
            </Channel>

            <Channel id="fx" volume={0.7}>
                <Audio id="fx1" src={loader.resources.fx1!.data as AudioBuffer} />
                <Audio id="fx2" src={loader.resources.fx2!.data as AudioBuffer} />
                <Audio id="fx3" src={loader.resources.fx3!.data as AudioBuffer} />
                <Audio id="fx4" src={loader.resources.fx4!.data as AudioBuffer} />
                <Audio id="fx5" src={loader.resources.fx5!.data as AudioBuffer} />
                <Audio id="fx6" src={loader.resources.fx6!.data as AudioBuffer} />
                <Audio id="fx7" src={loader.resources.fx7!.data as AudioBuffer} />
                <Audio id="fx8" src={loader.resources.fx8!.data as AudioBuffer} />
                <Audio id="fx9" src={loader.resources.fx9!.data as AudioBuffer} />
            </Channel>

            <Channel id="fg" volume={1}>
                <Audio id="fg1" src={loader.resources.fg1!.data as AudioBuffer} />
                <Audio id="fg2" src={loader.resources.fg2!.data as AudioBuffer} />
                <Audio id="fg3" src={loader.resources.fg3!.data as AudioBuffer} />
                <Audio id="fg4" src={loader.resources.fg4!.data as AudioBuffer} />
                <Audio id="fg5" src={loader.resources.fg5!.data as AudioBuffer} />
            </Channel>

            <Audio id="bg4" channelId="bg" src={loader.resources.bg4!.data as AudioBuffer} />
            <Audio id="bg5" channelId="bg" src={loader.resources.bg5!.data as AudioBuffer} loop />

            <MasterControls title="Master" />

            <Row>
                <Column lg={4}>
                    <ChannelControls title="Background" channel="bg" />
                </Column>
                <Column lg={4}>
                    <ChannelControls title="Effect" channel="fx" />
                </Column>
                <Column lg={4}>
                    <ChannelControls title="Foreground" channel="fg" />
                </Column>
            </Row>
        </div>
    );
};

App.displayName = 'App';

export { App };
