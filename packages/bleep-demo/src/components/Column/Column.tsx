/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { HTMLAttributes, ReactNode, useMemo } from 'react';

import { columns, gutter } from '../../theme';

const createStyle = (
    xsCols: number,
    smCols?: number,
    mdCols?: number,
    lgCols?: number
): {
    root: SerializedStyles[];
} => {
    const root = css`
        box-sizing: border-box;
        flex-shrink: 0;
        padding: 0 ${gutter};
        width: ${(xsCols / columns) * 100}%;
    `;

    const blank = css``;

    const sm = !smCols ? blank : css`
        @media (min-width: 576px) {
            width: ${(xsCols / columns) * 100}%;
        }
    `;

    const md = !mdCols ? blank : css`
        @media (min-width: 768px) {
            width: ${(mdCols / columns) * 100}%;
        }
    `;

    const lg = !lgCols ? blank : css`
        @media (min-width: 992px) {
            width: ${(lgCols / columns) * 100}%;
        }
    `;

    return {
        root: [root, sm, md, lg]
    };
};

export interface ColumnProps extends HTMLAttributes<HTMLDivElement> {
    /**
     * Column size `xs` breakpoint.
     * @default 12
     */
    xs?: number;

    /**
     * Column size `sm` breakpoint.
     */
    sm?: number;

    /**
     * Column size `md` breakpoint.
     */
    md?: number;

    /**
     * Column size `lg` breakpoint.
     */
    lg?: number;

    /** @ignore */
    children?: ReactNode;
}

const Column = ({
    xs = 12,
    sm,
    md,
    lg,
    children,
    ...other
}: ColumnProps): JSX.Element => {
    const style = useMemo(() => createStyle(xs, sm, md, lg), [xs, sm, md, lg]);

    return (
        <div css={style.root} {...other}>
            {children}
        </div>
    );
};

Column.displayName = 'Column';

export { Column };
