/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { useCallback, useMemo, useState } from 'react';
import { Audio, PlayStateListener } from '@heinzelman-labs/bleep-core';
import { useEvent } from '@heinzelman-labs/bleep-react';

import { icons, IconButton, IconButtonProps } from '../IconButton';

const createStyle = (): {
    root: SerializedStyles[];
} => {
    const root = css`

    `;

    return {
        root: [root]
    };
};

export interface StopProps extends Omit<IconButtonProps, 'disabled' | 'iconProps'> {
    target: Audio;
}

const Stop = ({
    target,
    ...other
}: StopProps): JSX.Element | null => {
    const [started, setStarted] = useState<boolean>(Boolean(target?.started));
    const style = useMemo(() => createStyle(), []);

    const clickHandler = useCallback(() => {
        target?.stop();
    }, [target]);

    const playStateHandler = useCallback<PlayStateListener>(({ started }) => {
        setStarted(started);
    }, []);

    useEvent(target, 'play-state', playStateHandler);

    return !target ? null : (
        <IconButton
            css={style.root}
            onClick={clickHandler}
            disabled={!started}
            iconProps={icons.stop}
            title="Stop"
            {...other}
        />
    );
};

Stop.displayName = 'Stop';

export { Stop };
