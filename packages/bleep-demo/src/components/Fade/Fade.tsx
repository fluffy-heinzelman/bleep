/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { AriaAttributes, useCallback, useMemo, useState } from 'react';
import { EventTarget, FadeListener } from '@heinzelman-labs/bleep-core';
import { useEvent } from '@heinzelman-labs/bleep-react';

import { palettes } from '../../theme';
import { icons, IconButton, IconButtonProps } from '../IconButton';

const createStyle = (): {
    root: SerializedStyles[];
    bar: SerializedStyles[];
} => {
    const root = css`
        position: relative;
        z-index: 1;
        /* Adjust default icon size */
        font-size: 0.85rem;
    `;

    const bar = css`
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) rotate(-90deg);
        stroke: ${palettes.primary.contrast.main};
    `;

    return {
        root: [root],
        bar: [bar]
    };
};

export interface FadeProps extends Omit<IconButtonProps, 'disabled' | 'iconProps'> {
    target?: EventTarget;
}

const Fade = ({
    target,
    ...other
}: FadeProps): JSX.Element | null => {
    const [fade, setFade] = useState<number>(target?.fade || 1);
    const [to, setTo] = useState<number>(target?.fadeTo || 0);
    const style = useMemo(() => createStyle(), []);

    const clickHandler = useCallback(() => {
        if (to === 1) target?.fadeOut();
        else target?.fadeIn();
    }, [target, to]);

    const fadeHandler = useCallback<FadeListener>(({ fade, to }) => {
        setFade(fade);
        setTo(to);
    }, []);

    useEvent(target, 'fade', fadeHandler);

    const radius = 12;
    const size = radius * 2;
    const strokeWidth = 1.25;

    const barRadius = radius - strokeWidth / 2;
    const barCircumference = 2 * Math.PI * barRadius;
    const barDashoffset = barCircumference * (1 - fade);

    const percentageFade = Math.round(fade * 100);
    const ariaProps = useMemo<Partial<AriaAttributes>>(
        () => ({
            'aria-valuemin': 0,
            'aria-valuemax': 100,
            'aria-valuenow': percentageFade
        }),
        [percentageFade]
    );

    return !target ? null : (
        <IconButton
            css={style.root}
            onClick={clickHandler}
            iconProps={to === 1 ? icons.fadeOut : icons.fadeIn}
            title={to === 1 ? 'Fade out' : 'Fade in'}
            {...other}
        >
            <svg
                css={style.bar}
                role="progressbar"
                width={size}
                height={size}
                viewBox={`0 0 ${size} ${size}`}
                {...ariaProps}
            >
                <circle
                    cx={size / 2}
                    cy={size / 2}
                    r={barRadius}
                    fill="none"
                    strokeWidth={strokeWidth}
                    strokeDasharray={barCircumference}
                    strokeDashoffset={barDashoffset}
                />
            </svg>
        </IconButton>
    );
};

Fade.displayName = 'Fade';

export { Fade };
