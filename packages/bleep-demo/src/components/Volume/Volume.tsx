/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { ChangeEvent, useCallback, useMemo, useState } from 'react';
import { EventTarget, VolumeListener } from '@heinzelman-labs/bleep-core';
import { useEvent } from '@heinzelman-labs/bleep-react';

import { SomePartial } from '../../types';
import { Range, RangeProps } from '../Range';

const createStyle = (): {
    root: SerializedStyles[];
} => {
    const root = css`

    `;

    return {
        root: [root]
    };
};

export interface VolumeProps extends
    SomePartial<Omit<RangeProps, 'min' | 'max' | 'value' | 'step'>, 'name'> {
    target?: EventTarget;
}

const Volume = ({
    name,
    onChange,
    target,
    ...other
}: VolumeProps): JSX.Element | null => {
    const style = useMemo(() => createStyle(), []);
    const [volume, setVolume] = useState<number>(target?.volume || 0);

    const changeHandler = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if (onChange) onChange(event);
        /* eslint-disable-next-line no-param-reassign */
        if (target) target.volume = Number(event.target.value);
    }, [onChange, target]);

    const volumeHandler = useCallback<VolumeListener>(({ volume }) => {
        setVolume(volume);
    }, []);

    useEvent(target, 'volume', volumeHandler);

    return !target ? null : (
        <Range
            css={style.root}
            name={name || `volume-${target?.id || ''}`}
            min={0}
            max={1}
            value={volume}
            step={0.01}
            onChange={changeHandler}
            {...other}
        />
    );
};

export { Volume };
