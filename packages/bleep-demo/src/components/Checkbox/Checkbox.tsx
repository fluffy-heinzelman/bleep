/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { InputHTMLAttributes, useMemo } from 'react';
import { svgPathData, width, height } from '@fortawesome/free-solid-svg-icons/faCheck';

import { borderRadius, focusShadow, palettes } from '../../theme';

const checkIcon = (color = '#000'): string => (`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ${width} ${height}">
        <path
            fill="${color}"
            stroke="${color}"
            d="${svgPathData}"
        />
    </svg>
`);

const createStyle = (): {
    input: SerializedStyles[];
} => {
    const icon = checkIcon(palettes.primary.contrast.main);

    const input = css`
        width: 1rem;
        height: 1rem;
        vertical-align: middle;
        background-color: ${palettes.neutral.lighter};
        background-repeat: no-repeat;
        background-position: center;
        background-size: 65%;
        border: 0;
        border-radius: ${borderRadius};
        appearance: none;
        cursor: pointer;

        &:focus {
            outline: 0;
            box-shadow:  ${focusShadow};
        }

        &:checked {
            border-color: ${palettes.primary.main};
            background-color: ${palettes.primary.main};
            background-image: url("data:image/svg+xml,${escape(icon)}");
        }

        &:disabled {
            pointer-events: none;
            filter: none;
            opacity: 0.8;
        }
    `;

    return {
        input: [input]
    };
};

export type CheckboxProps = Omit<InputHTMLAttributes<HTMLInputElement>, 'type'>;

const Checkbox = ({
    ...other
}: CheckboxProps): JSX.Element => {
    const style = useMemo(() => createStyle(), []);
    const { name = '' } = other;

    return (
        <input
            css={style.input}
            id={name}
            {...other}
            type="checkbox"
        />
    );
};

Checkbox.displayName = 'Checkbox';

export { Checkbox };
