/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { useCallback, useMemo, useState } from 'react';
import { EventTarget, SuspendStateListener } from '@heinzelman-labs/bleep-core';
import { useEvent } from '@heinzelman-labs/bleep-react';

import { icons, IconButton, IconButtonProps } from '../IconButton';

const createStyle = (): {
    root: SerializedStyles[];
} => {
    const root = css`

    `;

    return {
        root: [root]
    };
};

export interface ResumeProps extends Omit<IconButtonProps, 'disabled' | 'iconProps'> {
    target?: EventTarget;
}

const Resume = ({
    target,
    ...other
}: ResumeProps): JSX.Element | null => {
    const [suspended, setSuspended] = useState<boolean>(Boolean(target?.suspended));
    const style = useMemo(() => createStyle(), []);

    const clickHandler = useCallback(() => {
        if (target && suspended) target.resume();
    }, [target, suspended]);

    const suspendStateHandler = useCallback<SuspendStateListener>(({ suspended }) => {
        setSuspended(suspended);
    }, []);

    useEvent(target, 'suspend-state', suspendStateHandler);

    return !target ? null : (
        <IconButton
            css={style.root}
            onClick={clickHandler}
            disabled={!suspended}
            iconProps={icons.resume}
            title="Resume"
            {...other}
        />
    );
};

Resume.displayName = 'Resume';

export { Resume };
