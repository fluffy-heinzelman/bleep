/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { Fragment, LabelHTMLAttributes, ReactNode, useMemo } from 'react';

const createStyle = (
    props: Required<Pick<LabelProps, 'position'>>
): {
    root: SerializedStyles[];
} => {
    const { position } = props;

    const root = css`
        display: inline-block;
        margin-bottom: 1rem;
        & > span {
            display: inline-block;
            vertical-align: middle;
        }
    `;

    const blank = css``;

    const top = position !== 'top' ? blank : css`
        & > span {
            display: block;
            margin-bottom: 0.25rem;
        }
    `;

    const right = position !== 'right' ? blank : css`
        & > span {
            margin-left: 0.5rem;
        }
    `;

    const left = position !== 'left' ? blank : css`
        & > span {
            margin-right: 0.5rem;
        }
    `;

    return {
        root: [root, top, right, left]
    };
};

export interface LabelProps extends LabelHTMLAttributes<HTMLLabelElement> {
    /**
     * Label string.
     */
    label?: string;

    /**
     * Label position.
     * @default right
     */
    position?: 'top' | 'right' | 'left';

    /** @ignore */
    children?: ReactNode;
}

const Label = ({
    label: labelProp,
    position = 'right',
    children,
    ...other
}: LabelProps): JSX.Element => {
    const style = useMemo(() => createStyle({ position }), [position]);

    if (!labelProp) {
        return (
            <Fragment>
                {children}
            </Fragment>
        );
    }

    const label = <span>{labelProp}</span>;

    return (
        <label
            css={style.root}
            {...other}
        >
            { position !== 'right' && label }
            {children}
            { position === 'right' && label }
        </label>
    );
};

Label.displayName = 'Label';

export { Label };
