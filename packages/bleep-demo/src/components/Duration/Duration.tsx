import React, { ReactElement } from 'react';
import { Audio } from '@heinzelman-labs/bleep-core';

export interface DurationProps {
    target?: Audio;
}

const Duration = ({ target }: DurationProps): ReactElement => {
    const duration = target?.duration || 0;
    return <>{new Date(duration * 1000).toISOString().substr(14, 5)}</>;
};

export { Duration };
