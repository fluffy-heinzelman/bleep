/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, {
    InputHTMLAttributes,
    HTMLAttributes,
    MouseEvent,
    OutputHTMLAttributes,
    useCallback,
    useMemo,
    useState
} from 'react';
import { svgPathData, width, height } from '@fortawesome/free-solid-svg-icons/faGripVertical';

import { borderRadius, focusShadow, focusSize, palettes } from '../../theme';

const handleIcon = (color = '#000'): string => (`
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ${width} ${height}">
        <path
            fill="${color}"
            stroke="${color}"
            d="${svgPathData}"
        />
    </svg>
`);

const createStyle = (): {
    root: SerializedStyles[];
    input: SerializedStyles[];
    output: SerializedStyles[];
    thumbWidth: number;
} => {
    const trackHeight = '0.5rem';
    const thumbWidth = '1.2rem';
    const thumbHeight = '1rem';
    const icon = handleIcon(palettes.primary.contrast.main);

    const root = css`
        display: inline-block;
        position: relative;
        z-index: 1;
    `;

    const input = css`
        width: 100%;
        height: ${parseInt(thumbHeight, 10) + parseInt(focusSize, 10) * 2}rem;
        vertical-align: middle;
        padding: 0;
        background-color: transparent;
        border: 0;
        appearance: none;

        &:focus {
            outline: none;

            &::-webkit-slider-thumb {
                box-shadow: ${focusShadow};
            }

            &::-moz-range-thumb {
                box-shadow: ${focusShadow};
            }
        }

        &::-moz-focus-outer {
            border: 0;
        }

        &::-webkit-slider-thumb {
            width: ${thumbWidth};
            height: ${thumbHeight};
            cursor: pointer;
            margin-top: -${(parseInt(thumbHeight, 10)) / 4}rem;
            background-color: ${palettes.primary.main};
            background-image: url("data:image/svg+xml,${escape(icon)}");
            background-repeat: no-repeat;
            background-position: center;
            background-size: 25%;
            border: 0;
            border-radius: ${borderRadius};
            appearance: none;
        }

        &::-moz-range-thumb {
            width: ${thumbWidth};
            height: ${thumbHeight};
            cursor: pointer;
            background-color: ${palettes.primary.main};
            background-image: url("data:image/svg+xml,${escape(icon)}");
            background-repeat: no-repeat;
            background-position: center;
            background-size: 25%;
            border: 0;
            border-radius: ${borderRadius};
            appearance: none;
        }

        &::-webkit-slider-runnable-track {
            width: 100%;
            height: ${trackHeight};
            background-color: ${palettes.neutral.lighter};
            border-color: transparent;
            border-radius: ${borderRadius};
        }

        &::-moz-range-track {
            width: 100%;
            height: ${trackHeight};
            background-color: ${palettes.neutral.lighter};
            border-color: transparent;
            border-radius: ${borderRadius};
        }
    `;

    const output = css`
        position: absolute;
        z-index: 1;
        bottom: 8px;
        left: 0;
        min-width: 1rem;
        transform: translate(-50%, -100%);
        border-radius: ${borderRadius};
        color: ${palettes.primary.contrast.main};
        background: ${palettes.primary.main};
        padding: 0.1em 0.5em;
        font-size: 0.8em;
        word-wrap: break-word;
        text-align: center;
        user-select: none;
        opacity: 0;
        transition: opacity 250ms ease-in-out;
        pointer-events: none;

        &::after {
            content: "";
            position: absolute;
            width: 0;
            height: 0;
            border-top: 5px solid ${palettes.primary.main};
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            bottom: -4px;
            left: 50%;
            transform: translateX(-50%)
        }
    `;

    return {
        root: [root],
        input: [input],
        output: [output],
        thumbWidth: 16 * parseInt(thumbWidth, 10)
    };
};

export type RangeInputProps =
    Omit<InputHTMLAttributes<HTMLInputElement>, 'max' | 'min' | 'name' | 'step' | 'type' | 'value'>;

export type RangeOutputProps =
    OutputHTMLAttributes<HTMLOutputElement>;

export interface RangeProps extends
    HTMLAttributes<HTMLDivElement> {
    /**
     * The greatest value in the range of permitted values.
     * @default 100
     */
    max?: number;

    /**
     * The lowest value in the range of permitted values.
     * @default 0
     */
    min?: number;

    /**
     * Name of the `Range` instance that can be used to
     * associate the `<output>` with the `<input>` element.
     */
    name: string;

    /**
     * The interval between legal values.
     */
    step?: number;

    /**
     * Selected value.
     * @default 0
     */
    value?: number;

    /**
     * Input element HTML attributes.
     */
    inputProps?: RangeInputProps;

    /**
     * Output element HTML attributes.
     */
    outputProps?: RangeOutputProps;
}

const Range = ({
    max = 100,
    min = 0,
    name,
    step,
    value = 0,
    inputProps,
    outputProps,
    ...other
}: RangeProps): JSX.Element => {
    const style = useMemo(() => createStyle(), []);
    const [opacity, setOpacity] = useState(0);
    const { onChange = () => {}, onMouseDown, onMouseUp } = inputProps || {};

    const mouseDownHandler = useCallback((e: MouseEvent<HTMLInputElement>) => {
        if (onMouseDown) onMouseDown(e);
        setOpacity(1);
    }, [onMouseDown]);

    const mouseUpHandler = useCallback((e: MouseEvent<HTMLInputElement>) => {
        if (onMouseUp) onMouseUp(e);
        setOpacity(0);
    }, [onMouseUp]);

    const left = ((value - min) * 100) / (max - min);
    const outputStyle: SerializedStyles[] = style.output.concat([
        css`
            left: calc(${left}% + (${(style.thumbWidth / 2) - left * 0.15}px));
            opacity: ${opacity}
        `
    ]);

    return (
        <div
            css={style.root}
            {...other}
        >
            <input
                css={style.input}
                type="range"
                id={name}
                max={max}
                min={min}
                step={step}
                value={value}
                onChange={onChange}
                {...inputProps}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
            />
            <output
                css={outputStyle}
                htmlFor={name}
                {...outputProps}
            >
                {value}
            </output>
        </div>
    );
};

Range.displayName = 'Range';

export { Range };
