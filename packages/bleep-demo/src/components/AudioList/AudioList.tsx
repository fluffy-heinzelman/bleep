/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { useCallback, useMemo, useState } from 'react';
import { Audio, AudioAddedListener, AudioRemovedListener, Channel } from '@heinzelman-labs/bleep-core';
import { useEvent } from '@heinzelman-labs/bleep-react';

import { AudioItem } from '../AudioItem';
import { palettes } from '../../theme';

const createStyle = (): {
    root: SerializedStyles[];
    tableWrapper: SerializedStyles[];
    table: SerializedStyles[];
    link: SerializedStyles[];
} => {
    const root = css`
        font-size: 0.875rem;

        th {
            padding: 0.375rem;
            text-align: left;

            &:first-of-type {
                padding-left: 0;
            }

            &:last-of-type {
                padding-right: 0;
            }
        }
    `;

    const tableWrapper = css`
        /* Small space between last row and table scrollbar */
        padding-bottom: 5px;
        margin-bottom: -5px;
        overflow-x: auto;
    `;

    const table = css`
        width: 100%;
        white-space: nowrap;
    `;

    const link = css`
        color: ${palettes.neutral.lighter};
    `;

    return {
        root: [root],
        tableWrapper: [tableWrapper],
        table: [table],
        link: [link]
    };
};

export interface AudioListProps {
    target?: Channel;
}

const AudioList = ({ target }: AudioListProps): JSX.Element => {
    const style = useMemo(() => createStyle(), []);
    const [audios, setAudios] = useState<Audio[]>(() => target?.getAudios() || []);

    const audioAddedHandler = useCallback<AudioAddedListener>(({ target }) => {
        setAudios(target.getAudios());
    }, []);

    const audioRemovedHandler = useCallback<AudioRemovedListener>(({ target }) => {
        setAudios(target.getAudios());
    }, []);

    useEvent(target, 'audio-added', audioAddedHandler);
    useEvent(target, 'audio-removed', audioRemovedHandler);

    const rows = useMemo(() => audios.map((audio) => {
        return <AudioItem key={audio.id} target={audio} />;
    }), [audios]);

    return (
        <div css={style.root}>
            <h3>Audio</h3>
            <div css={style.tableWrapper}>
                <table css={style.table}>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Time</th>
                            <th>Loop</th>
                            <th>Mute</th>
                            <th>Volume</th>
                            <th />
                        </tr>
                    </thead>
                    <tbody>
                        {rows}
                    </tbody>
                </table>
            </div>

            <a
                css={style.link}
                href="https://gitlab.com/fluffy-heinzelman/bleep/-/blob/develop/packages/bleep-demo/ASSETS.md"
                target="blank"
                title="Audio Copyright Policies"
                rel="noopener noreferrer"
            >
                Audio Copyright
            </a>
        </div>
    );
};

AudioList.displayName = 'AudioList';

export { AudioList };
