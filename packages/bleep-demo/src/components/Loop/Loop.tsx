/** @jsx jsx */
import { css, jsx, SerializedStyles } from '@emotion/core';
import React, { ChangeEvent, useCallback, useMemo, useState } from 'react';
import { Audio, LoopStateListener } from '@heinzelman-labs/bleep-core';
import { useEvent } from '@heinzelman-labs/bleep-react';

import { Checkbox, CheckboxProps } from '../Checkbox';

const createStyle = (): {
    root: SerializedStyles[];
} => {
    const root = css`

    `;

    return {
        root: [root]
    };
};

export interface LoopProps extends Omit<CheckboxProps, 'checked'> {
    target?: Audio;
}

const Loop = ({
    target,
    ...other
}: LoopProps): JSX.Element | null => {
    const style = useMemo(() => createStyle(), []);
    const [loop, setLoop] = useState<boolean>(Boolean(target?.loop));
    const { onChange } = other;

    const changeHandler = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if (onChange) onChange(event);
        /* eslint-disable-next-line no-param-reassign */
        if (target) target.loop = event.target.checked;
    }, [onChange, target]);

    const loopStateHandler = useCallback<LoopStateListener>(({ loop }) => {
        setLoop(loop);
    }, []);

    useEvent(target, 'loop-state', loopStateHandler);

    return !target ? null : (
        <Checkbox
            css={style.root}
            name={`loop-${target.id}`}
            {...other}
            checked={loop}
            onChange={changeHandler}
        />
    );
};

Loop.displayName = 'Loop';

export { Loop };
