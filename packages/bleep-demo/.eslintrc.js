module.exports = {
    rules: {
        // todo enable if deciding to hold demo fully accessible
        'jsx-a11y/control-has-associated-label': 'off',
        // todo enable if deciding to hold demo fully accessible
        'jsx-a11y/label-has-associated-control': 'off'
    }
};
