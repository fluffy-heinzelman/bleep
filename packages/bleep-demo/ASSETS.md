# Asset Creators & Copyright

## Background Sounds

Bleep Id | Artist | Title | Download Origin | License
--------|--------|-------|-------------------|--------
bg1 | sawsquarenoise | Stage 3 | [freemusicarchive.org](https://freemusicarchive.org/music/sawsquarenoise) | [Creative Commons](https://creativecommons.org/licenses/by/4.0/)
bg2 | David Renda | 8 Bit Menu | [fesliyanstudios.com](https://www.fesliyanstudios.com/) | [FesliyanStudios Music Policy](https://www.fesliyanstudios.com/policy)
bg3 | David Renda | Arcade Kid | [fesliyanstudios.com](https://www.fesliyanstudios.com/) | [FesliyanStudios Music Policy](https://www.fesliyanstudios.com/policy)
bg4 | David Fesliyan | Retro Platforming | [fesliyanstudios.com](https://www.fesliyanstudios.com/) | [FesliyanStudios Music Policy](https://www.fesliyanstudios.com/policy)
bg5 | SketchyLogic | Main Theme | [opengameart.org](https://opengameart.org/content/hungry-dino-9-chiptune-tracks-10-sfx) | [SketchyLogic Music Policy](https://opengameart.org/content/hungry-dino-9-chiptune-tracks-10-sfx)

## Effect Sounds

Bleep Id | Artist | Title | Download Origin | License
--------|--------|-------|-------------------|--------
fx1 | audiosoundclips.com | 8bitgame | [audiosoundclips.com](https://audiosoundclips.com/8-bit-game-sound-effects-sfx/) | [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/)
fx2 | audiosoundclips.com | 8bitgame3 | [audiosoundclips.com](https://audiosoundclips.com/8-bit-game-sound-effects-sfx/) | [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/)
fx3 | audiosoundclips.com | 8bitgame4 | [audiosoundclips.com](https://audiosoundclips.com/8-bit-game-sound-effects-sfx/) | [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/)
fx4 | audiosoundclips.com | 8bitgame5 | [audiosoundclips.com](https://audiosoundclips.com/8-bit-game-sound-effects-sfx/) | [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/)
fx5 | audiosoundclips.com | 8bitgame6 | [audiosoundclips.com](https://audiosoundclips.com/8-bit-game-sound-effects-sfx/) | [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/)
fx6 | audiosoundclips.com | 8bitgame7 | [audiosoundclips.com](https://audiosoundclips.com/8-bit-game-sound-effects-sfx/) | [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/)
fx7 | audiosoundclips.com | 8bitgame8 | [audiosoundclips.com](https://audiosoundclips.com/8-bit-game-sound-effects-sfx/) | [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/)
fx8 | audiosoundclips.com | 8bitgame9 | [audiosoundclips.com](https://audiosoundclips.com/8-bit-game-sound-effects-sfx/) | [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/)
fx9 | audiosoundclips.com | 8bitgame10 | [audiosoundclips.com](https://audiosoundclips.com/8-bit-game-sound-effects-sfx/) | [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/)

## Foreground Sounds

Bleep Id | Artist | Title | Download Origin | License
--------|--------|-------|-------------------|--------
fg1 | Bart Kelsey | Arrr | [opengameart.org](https://opengameart.org/content/arr) | Not provided
fg2 | Ruben Wardy | Oh my! | [opengameart.org](https://opengameart.org/content/oh-my-help-me-posh-english-high-pitched) | Not provided
fg3 | n3b | Doh! | [opengameart.org](https://opengameart.org/content/doh) | Not provided
fg4 | Bart Kelsey | Hiya | [opengameart.org](https://opengameart.org/content/24-cute-fighting-game-voice-recordings) | [Bart Kelsey Music Policy](https://opengameart.org/content/24-cute-fighting-game-voice-recordings)
fg5 | Bart Kelsey | I'm invincible | [opengameart.org](https://opengameart.org/content/24-cute-fighting-game-voice-recordings) | [Bart Kelsey Music Policy](https://opengameart.org/content/24-cute-fighting-game-voice-recordings)
