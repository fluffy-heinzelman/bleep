# Bleep - Demo

*This is still work-in-progress!*

A demo of the default `Bleep` setup with 3 sub-channels.

## Demo

Have fun playing with the [Demo](https://fluffy-heinzelman.gitlab.io/bleep/).

## Credits

The development of Bleep would have been half the fun without the hard work of some great music artists and game enthusiasts. Special thanks therefore go to, among others:

* [FesliyanStudios](https://www.fesliyanstudios.com)
* [sawsquarenoise](http://rolemusic.sawsquarenoise.com/p/sawsquarenoise.html)
* [OpenGameArt](https://opengameart.org/)

## License

All content located in the "assets" directory of this package is subject to the licenses of the respective authors.

* [Background Sounds](./ASSETS.md#background-sounds)
* [Effect Sounds](./ASSETS.md#effect-sounds)
* [Foreground Sounds](./ASSETS.md#foreground-sounds)

Bleep software is [MIT licensed](./LICENSE.md).
