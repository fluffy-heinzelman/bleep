const { basename, resolve } = require('path');
const { getIfUtils, removeEmpty } = require('webpack-config-utils');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const devIP = require('dev-ip');
const getPortSync = require('get-port-sync')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// Set true to find out which loader is causing deprecation
// warnings.
process.traceDeprecation = false;

function crossEnvArg(name, defaultValue = undefined) {
    return typeof process.env[name] !== 'undefined' ? process.env[name].trim() : defaultValue;
}

const nodeEnv = crossEnvArg('NODE_ENV', 'development');
const server = crossEnvArg('SERVER');

const ip = devIP()[0];
const port = getPortSync();

const srcPath = resolve('src');
const buildPath = resolve('build');
const jsEntryPath = resolve(srcPath, 'index.tsx');
const htmlEntryPath = resolve(srcPath, 'index.html');
const fontPath = resolve('../../node_modules/@fontsource');
const title = 'Bleep';

const { ifProduction, ifWpServer, ifBsServer } = getIfUtils({
    production: nodeEnv === 'production',
    wpServer: server === 'webpack',
    bsServer: server === 'browsersync'
}, ['production', 'wpServer', 'bsServer']);

const isProduction = ifProduction(true, false);
const isWpServer = ifWpServer(true, false);
const isBsServer = ifBsServer(true, false);
const isServer = isWpServer || isBsServer;
const useSourceMap = !isProduction || isServer;

function wpConfig() {
    let config = {};

    config.target = 'web';
    config.watch = isServer;
    config.cache = isServer;
    config.mode = ifProduction('production', 'development');
    config.devtool = useSourceMap ? 'cheap-source-map' : undefined;
    config.context = srcPath;

    // For BrowserSync / webpack-hot-middleware (WDM) usage only:
    // The Client HMR runtime script. This connects to the server
    // to receive notifications when the bundle rebuilds and then
    // updates our client bundle accordingly.
    let hmrRuntime;

    if (isBsServer) {
        // Few options can be passed to influence what gets shown
        // in browser console on hot updates, errors and warnings.
        // We could also pass a "silenced" stats object to WDM but
        // it would affect terminal output, browser console output,
        // probably also stats.json export. In my case, I just
        // want to suppress eslint warnings/errors in browser
        // console while still showing them in terminal. Best
        // option seems to pass quiet=true here ...
        const noInfo = false;
        const quiet = true;

        hmrRuntime = '';
        hmrRuntime += `webpack-hot-middleware/client?http://${ip}:${port}/__webpack_hmr`;
        hmrRuntime += '&reload=true';
        hmrRuntime += `&noInfo=${noInfo}`;
        hmrRuntime += `&quiet=${quiet}`;
    }

    config.entry = {
        main: removeEmpty([
            hmrRuntime,
            jsEntryPath
        ])
    };

    config.resolve = {
        modules: [
            'node_modules',
            srcPath
        ],
        extensions: ['.ts', '.tsx', '.js'],
        descriptionFiles: ['package.json']
    };

    config.optimization = {
        minimize: isProduction
    };

    config.output = removeEmpty({
        path: resolve(buildPath, nodeEnv),
        publicPath: '',
        filename: ifProduction('js/[name].[hash].js', 'js/[name].js'),
        chunkFilename: ifProduction('js/[name].[hash].js', 'js/[name].js'),
        hotUpdateMainFilename: ifWpServer('js/hot-update.[hash].js'),
        hotUpdateChunkFilename: ifWpServer('js/hot-update.[id].[hash].js'),
        pathinfo: false
    });

    config.module = {
        strictExportPresence: true
    };

    config.module.rules = [
        {
            parser: {
                requireEnsure: false
            }
        },
        {
            oneOf: [
                {
                    test: /\.(ts|tsx|js|mjs|jsx)?$/,
                    include: srcPath,
                    use: [
                        {
                            loader: require.resolve('babel-loader'),
                            options: {
                                cacheDirectory: true,
                                cacheCompression: isProduction,
                                rootMode: 'upward-optional'
                            }
                        }
                    ]
                },

                {
                    test: /\.(ts|tsx|js|mjs|jsx)?$/,
                    use: [
                        {
                            loader: require.resolve('babel-loader'),
                            options: {
                                babelrc: false,
                                configFile: false,
                                cacheDirectory: true,
                                cacheCompression: isProduction,
                                sourceMaps: false
                            }
                        }
                    ]
                },

                {
                    test: /\.css$/,
                    include: [ srcPath, fontPath ],
                    use: removeEmpty([
                        !isServer ? {
                            loader:  MiniCssExtractPlugin.loader,
                            options: {
                                publicPath: '../'
                            }
                        } : {
                            loader: require.resolve('style-loader'),
                            options: {}
                        },
                        {
                            loader: require.resolve('css-loader'),
                            options: {
                                sourceMap: useSourceMap
                            }
                        }
                    ])
                },

                {
                    test: /\.(woff2?)$/,
                    include: [ srcPath, fontPath ],
                    use: [
                        {
                            loader: require.resolve('file-loader'),
                            options: {
                                name: 'fonts/[name].[ext]'
                            }
                        }
                    ]
                },

                {
                    test: /\.(aac|m4a|mp3|ogg|oga|mp4|webm|wav)$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: require.resolve('file-loader'),
                            options: {
                                name: '[path][name].[ext]'
                            }
                        }
                    ]
                }
            ]
        }
    ];

    config.plugins = removeEmpty([
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(nodeEnv),
            'process.env.PUBLIC_URL': JSON.stringify('.')
        }),

        new HtmlWebpackPlugin({
            title: title,
            lang: 'en',
            direction: 'ltr',
            template: htmlEntryPath,
            filename: basename(htmlEntryPath),
            noscript: 'You need to enable JavaScript to run this app.',
            inject: false,
            hash: false,
            minify: {
                html5: true,
                minifyCSS: isProduction,
                minifyJS: isProduction,
                minifyURLs: isProduction,
                removeComments: true,
                removeCommentsFromCDATA: true,
                removeScriptTypeAttributes: true,
                collapseWhitespace: isProduction,
                conservativeCollapse: false,
                preserveLineBreaks: false,
                keepClosingSlash: true,
                processScripts: ['text/javascript']
            }
        }),

        isServer ? new webpack.HotModuleReplacementPlugin() : undefined,

        isServer ? new webpack.NamedModulesPlugin() : undefined,

        new MiniCssExtractPlugin({
            filename: ifProduction('css/[name].[hash].css', 'css/[name].css'),
            chunkFilename: ifProduction('css/[name].[hash].css', 'css/[name].css')
        })
    ]);

    // This option does not have any effect when using devServer Node API.
    config.stats = {
        assets: false,
        builtAt: false,
        colors: true,
        entrypoints: false,
        hash: false,
        modules: false,
        timings: false,
        version: false
    };

    config.devServer = ifWpServer({
        host: 'localhost',
        port: port,
        contentBase: config.output.path,
        publicPath: '', // [WDM] via WDM config object
        compress: true,
        historyApiFallback: true,
        hot: true,
        inline: true,
        noInfo: false, // [WDM] via HMR runtime script URL
        quiet: false, // [WDM] via HMR runtime script URL
        stats: config.stats, // [WDM] via WDM config object
        clientLogLevel: 'error'
    });

    config.performance = {
        hints: false
    };

    return removeEmpty(config);
}

function bsConfig() {
    const config = wpConfig();
    const compiler = webpack(config);
    const htmlEntryFile = basename(htmlEntryPath);

    // Dev middleware is for serving the files emitted from webpack.
    const devMiddlewareConfig = {
        publicPath: config.output.publicPath,
        index: htmlEntryFile,
        logLevel: 'error',
        headers: {
            'Access-Control-Allow-Origin': `http://${ip}:${port}`,
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
        },
        stats: config.stats
    };

    // Hot middleware is only concerned with the mechanisms to
    // connect a browser client to a webpack server & receive
    // updates. It will subscribe to changes from the server and
    // execute those changes using Webpack HMR API.
    const hotMiddlewareConfig = {};

    const middleware = [
        webpackDevMiddleware(compiler, devMiddlewareConfig),
        webpackHotMiddleware(compiler, hotMiddlewareConfig)
    ];

    return {
        host: ip,
        port,
        open: true,
        cors: true,
        server: {
            baseDir: config.output.path,
            index: htmlEntryFile,
            middleware
        },
        files: [
            `${config.output.path}/*.html`
        ]
    }
}

module.exports = isBsServer ? bsConfig() : wpConfig;
