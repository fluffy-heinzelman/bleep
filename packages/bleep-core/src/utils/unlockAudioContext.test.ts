import { fireEvent, waitFor } from 'test-utils';

import { unlockAudioContext } from './unlockAudioContext';
import { webAudioContext } from './webAudioContext';

describe('unlockAudioContext', () => {
    let ctx: AudioContext;
    let stateSpy: jest.SpyInstance<AudioContextState, []>;
    let resumeSpy: jest.SpyInstance<Promise<void>, []>;

    beforeEach(() => {
        // AudioContext mock.
        /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
        ctx = webAudioContext!;

        // Mimic Chrome/Safari, where `AudioContext` is initially
        // locked and a user interaction is required to unlock
        // the AudioContext.
        stateSpy = jest
            .spyOn(ctx, 'state', 'get')
            .mockReturnValue('suspended');

        // Call `unlockAudioContext` helper, which listens to
        // user interactions in order to unlock the AudioContext.
        // AudioContext is mocked, so we expect the helper not
        // to throw any exception for now.
        expect(() => { unlockAudioContext(ctx); }).not.toThrow();

        // Spy on AudioContext.resume, which must be called by
        // the helper on first user interaction.
        resumeSpy = jest.spyOn(ctx, 'resume');
    });

    afterEach(() => {
        // Restore original implementations.
        stateSpy.mockRestore();
        resumeSpy.mockRestore();
    });

    it('unlocks after first click', () => {
        fireEvent.mouseDown(document.body);
        expect(resumeSpy).toHaveBeenCalledTimes(1);
    });

    it('unlocks after first touch', () => {
        fireEvent.touchStart(document.body);
        expect(resumeSpy).toHaveBeenCalledTimes(1);
    });

    it('unlocks after first key', () => {
        fireEvent.keyDown(document.body);
        expect(resumeSpy).toHaveBeenCalledTimes(1);
    });

    it('cleans up properly', async () => {
        fireEvent.mouseDown(document.body);

        // Wait for resume respectively cleanup.
        await waitFor(() => expect(resumeSpy).toHaveBeenCalledTimes(1));

        // Helper should now have removed all listeners. Fire
        // additional events.
        fireEvent.mouseDown(document.body);
        fireEvent.touchStart(document.body);
        fireEvent.keyDown(document.body);

        // Resume should still have been called only once.
        expect(resumeSpy).toHaveBeenCalledTimes(1);
    });
});
