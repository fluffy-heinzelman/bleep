declare global {
    interface Window {
        webkitAudioContext: typeof AudioContext;
    }
}

const AudioContextClass = window.AudioContext || window.webkitAudioContext;
const webAudioContext = AudioContextClass ? new AudioContextClass() : undefined;

export { webAudioContext };
