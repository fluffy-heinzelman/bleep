/* eslint-disable no-underscore-dangle */
export type RafUpdateFn = (deltaTime: number) => void;

class Raf {
    // The timestamp in milliseconds of the last run. Used to
    // compute the elapsed time between two frames.
    private _lastFrame = 0;

    // The minimum amount of time in milliseconds that must pass
    // since the last frame was executed before the next frame
    // can be executed.
    private _minDelay = 0;

    private _updateFn?: RafUpdateFn;
    private _started = false;
    private _startedAt = 0;
    private _handle?: number;

    constructor(maxFps?: number, updateFn?: RafUpdateFn) {
        if (maxFps) this.maxFps = maxFps;
        if (updateFn) this.updateFn = updateFn;
    }

    get started(): boolean {
        return this._started;
    }

    set maxFps(value: number) {
        if (value === 0) this.stop();
        else this._minDelay = 1000 / value;
    }

    set updateFn(value: RafUpdateFn) {
        this._updateFn = value;
    }

    start(): this {
        if (this._started) return this;
        this._started = true;
        this._lastFrame = 0;
        this._handle = window.requestAnimationFrame(this.frameHandler);
        return this;
    }

    stop(): this {
        if (this._handle) {
            window.cancelAnimationFrame(this._handle);
        }
        this._started = false;
        return this;
    }

    destroy(): void {
        this.stop();
        this._updateFn = undefined;
    }

    private frameHandler = (time: number): void => {
        this._handle = window.requestAnimationFrame(this.frameHandler);
        this._startedAt = this._startedAt || time;

        // Throttle if `maxFps` is set.
        const t = time - this._startedAt;
        if (t < this._lastFrame + this._minDelay) {
            return;
        }

        if (this._updateFn) {
            const deltaTime = !this._lastFrame ? 0 : t - this._lastFrame;
            this._updateFn(deltaTime);
        }

        this._lastFrame = t;
    };
}

export { Raf };
