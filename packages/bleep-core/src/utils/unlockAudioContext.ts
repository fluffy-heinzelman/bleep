const unlockAudioContext = (context: AudioContext): void => {
    if (context.state !== 'suspended') return;

    const events = [
        'touchstart',
        'touchend',
        'mousedown',
        'keydown'
    ];

    events.forEach(
        (event) => document.addEventListener(event, unlock, false)
    );

    function unlock(): void {
        context
            .resume()
            .then(clean);
    }

    function clean(): void {
        events.forEach(
            (event) => document.removeEventListener(event, unlock)
        );
    }
};

export { unlockAudioContext };
