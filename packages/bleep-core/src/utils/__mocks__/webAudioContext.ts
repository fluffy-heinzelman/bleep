import { AudioContext } from 'standardized-audio-context-mock';

const webAudioContext = new AudioContext();

export { webAudioContext };
