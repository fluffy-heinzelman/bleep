import { webAudioContext } from './webAudioContext';

// Indicates if the Web Audio API can be used.
// (1) The Web Audio API must be supported.
// (2) XMLHttpRequest Level 2 responseType 'arraybuffer' must be supported.
// (3) The application may not run locally.

const webAudioSupport = Boolean(
    webAudioContext &&
    window.ProgressEvent &&
    document.location.protocol !== 'file:'
);

export { webAudioSupport };
