import { webAudioContext } from './webAudioContext';

/**
 * Create an audio buffer for a dummy sinus tone.
 *
 * @param {number} [sec] Duration in seconds
 * @param {number} [frequ] Realtime frequency
 * @param {AudioContext} [context] AudioContext to be used
 * @throws {Error} Throws error if AudioContext is not available
 */
const sineToneBuffer = (sec = 2, frequ = 400, context?: AudioContext): AudioBuffer => {
    const ctx = context || webAudioContext;

    if (!ctx) {
        throw new Error('[@heinzelman-labs/bleep-core] AudioContext not available');
    }

    const angularFrequ = frequ * 2 * Math.PI;
    const sampleRate = 44100;
    const length = sec * sampleRate;
    const buffer = ctx.createBuffer(1, length, sampleRate);
    const arr = buffer.getChannelData(0);

    const sample = (num: number): number => {
        const time = num / sampleRate;
        const angle = time * angularFrequ;
        return Math.sin(angle);
    };

    let num: number;

    for (num = 0; num < length; num++) {
        arr[num] = sample(num);
    }

    return buffer;
};

export { sineToneBuffer };
