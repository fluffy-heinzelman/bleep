import { Audio } from './Audio';

export const isAudio = (value: any): value is Audio => {
    return value instanceof Audio;
};
