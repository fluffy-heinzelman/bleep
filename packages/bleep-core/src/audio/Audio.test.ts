/* eslint-disable no-new */
import { sineToneBuffer } from '../utils';
import { Audio } from './Audio';

describe('Audio', () => {
    it('throws error on duplicated identifiers', () => {
        expect(() => { new Audio('audio', sineToneBuffer()); }).not.toThrow();
        expect(() => { new Audio('audio', sineToneBuffer()); }).toThrow();
    });
});
