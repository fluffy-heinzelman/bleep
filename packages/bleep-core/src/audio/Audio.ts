/* eslint-disable no-underscore-dangle */
import { EventListener } from 'eventemitter3';

import { AudioEventTypes, Bus, BusEventTypes } from '../events';
import { Channel, Master } from '../channels';
import { Raf } from '../utils';

export interface AudioOptions {
    loop?: boolean;
    volume?: number;
    fadeTime?: number;
    fadeTo?: 0 | 1;
}

class Audio extends Bus<AudioEventTypes> {
    private static _instances: Map<string, Audio> = new Map();

    private _context?: AudioContext;
    private _gainNode?: GainNode;
    private _fadeNode?: GainNode;

    private _master?: Master;
    private _channel?: Channel;

    private _id: string;
    private _volume: number;
    private _muted = false;
    private _started = false;
    private _startedAt = 0;
    private _suspended = false;
    private _suspendedAt = 0;
    private _buffer: AudioBuffer | null;
    private _loop = false;
    private _sourceNode?: AudioBufferSourceNode;
    private _duration = 0;
    private _positionRaf: Raf;
    private _fadeRaf: Raf;
    private _fadeTo: 0 | 1;
    private _fadeTime = 2;
    private _velocity = 0.0005;

    constructor(id: string, buffer: AudioBuffer, options?: AudioOptions) {
        super();

        // Ensure unique identifiers.
        if (Audio._instances.has(id)) {
            throw new Error([
                `Audio with id "${id}" already exists.`,
                'Each audio must be instantiated with a unique id.'
            ].join(' '));
        }

        Audio._instances.set(id, this);

        const {
            loop = false,
            volume = 1,
            fadeTime = 2,
            fadeTo = 1
        } = options || {};

        this._buffer = buffer;
        this._loop = loop;
        this._duration = buffer.duration;
        this._id = id;
        this._volume = volume;
        this.fadeTime = fadeTime;
        this._fadeTo = fadeTo;

        this._bus.on('suspend-state', this.busSuspendStateHandler);

        this._positionRaf = new Raf(2, this.audioPositionHandler);
        this._fadeRaf = new Raf(Infinity, this.fadeHandler);
    }

    public static getAudio(audioId: string): Audio | undefined {
        return Array.from(Audio._instances.values()).find(
            (audio) => audio.id === audioId
        );
    }

    // internal (usually)
    public connect(node: AudioNode, context: AudioContext): this {
        if (this._context) {
            /* eslint-disable-next-line no-console */
            console.warn([
                '[@heinzelman-labs/bleep-core]',
                `Audio with id "${this.id}" already connected.`
            ].join(' '));
            return this;
        }

        this._master = Master.getMasterOfAudio(this.id);
        this._channel = Channel.getChannelOfAudio(this.id);
        this._context = context;

        this.gainNode.connect(this.fadeNode);
        this.gainNode.gain.value = this._muted ? 0 : this._volume;
        this.fadeNode.connect(node);

        return this;
    }

    // internal (usually)
    public disconnect(): this {
        if (!this._context) {
            /* eslint-disable-next-line no-console */
            console.warn([
                '[@heinzelman-labs/bleep-core]',
                `Audio with id "${this.id}" already disconnected.`
            ].join(' '));
            return this;
        }

        this._master = undefined;
        this._channel = undefined;
        this._context = undefined;

        this.fadeNode.disconnect();
        this.gainNode.disconnect();
        this.gainNode.gain.value = this._muted ? 0 : this._volume;

        return this;
    }

    private get context(): AudioContext {
        if (!this._context) {
            throw new Error([
                '[@heinzelman-labs/bleep-core]',
                `Audio with id "${this.id}" is not connected (context not set).`
            ].join(' '));
        }
        return this._context;
    }

    private get gainNode(): GainNode {
        if (!this._gainNode) {
            this._gainNode = this.context.createGain();
        }
        return this._gainNode;
    }

    private get fadeNode(): GainNode {
        if (!this._fadeNode) {
            this._fadeNode = this.context.createGain();
        }
        return this._fadeNode;
    }

    get id(): string {
        return this._id;
    }

    get fading(): boolean {
        return this._fadeRaf.started;
    }

    get fadingIn(): boolean {
        return this.fading && this._fadeTo === 1;
    }

    get fadingOut(): boolean {
        return this.fading && this._fadeTo === 0;
    }

    get fade(): number {
        return this.fadeNode.gain.value;
    }

    get fadeTo(): number {
        return this._fadeTo;
    }

    get fadeTime(): number {
        return this._fadeTime;
    }

    // sec
    set fadeTime(value: number) {
        this._fadeTime = value;
        this._velocity = 1 / (value * 1000);
    }

    get loop(): boolean {
        return this._loop;
    }

    set loop(value: boolean) {
        if (value === this._loop) return;
        this._loop = value;
        if (this._sourceNode) this._sourceNode.loop = value;
        this.emit('loop-state', { target: this, loop: value });
    }

    get muted(): boolean {
        return this._muted;
    }

    set muted(value: boolean) {
        if (value === this._muted) return;
        this._muted = value;
        this.gainNode.gain.value = value ? 0 : this.volume;
        this.emit('mute-state', { target: this, muted: value });
    }

    get volume(): number {
        return this._volume;
    }

    set volume(value: number) {
        this._volume = value;
        if (!this.muted) this.gainNode.gain.value = value;
        this.emit('volume', { target: this, volume: value });
    }

    get started(): boolean {
        return this._started;
    }

    get playing(): boolean {
        return this._started && !this._suspended;
    }

    get suspended(): boolean {
        return this._suspended;
    }

    get duration(): number {
        return this._duration;
    }

    get position(): number {
        if (this._started) {
            const position = (this._suspendedAt || this.context.currentTime) - this._startedAt;
            if (this._loop && position > this.duration) {
                this._startedAt = this.context.currentTime;
                return 0;
            }
            return position;
        }
        return 0;
    }

    public start(): this {
        if (this._started || this._suspended) return this;

        this.setSourceNode();

        this._started = true;
        this._startedAt = this.context.currentTime;

        this.emit('play-state', { target: this, started: this.started, playing: this.playing });

        if (this._master?.suspended || this._channel?.suspended) return this;

        this._sourceNode?.start(0, this.position);
        this._positionRaf.start();

        return this;
    }

    public stop(): this {
        if (!this._started) return this;

        this.unsetSourceNode();
        this._positionRaf.stop();

        const wasSuspended = this._suspended;
        this._started = false;
        this._suspended = false;
        this._startedAt = 0;
        this._suspendedAt = 0;

        if (wasSuspended) {
            this.emit('suspend-state', { target: this, suspended: false });
        }

        this.emit('play-state', { target: this, started: this.started, playing: this.playing });
        this.emit('position', { target: this, position: this.position });

        return this;
    }

    public suspend(): this {
        if (!this._started || this._suspended) return this;

        this.unsetSourceNode();
        this._positionRaf.stop();

        this._suspended = true;
        this._suspendedAt = this._suspendedAt || this.context.currentTime; // system timestamp takes precedence

        this.emit('suspend-state', { target: this, suspended: true });
        this.emit('play-state', { target: this, started: this.started, playing: this.playing });

        return this;
    }

    public resume(): this {
        if (!this._started || !this._suspended) return this;

        this._suspended = false;
        this.emit('suspend-state', { target: this, suspended: false });

        if (this._master?.suspended || this._channel?.suspended) return this;

        this.setSourceNode();
        this._startedAt += this.context.currentTime - this._suspendedAt;
        this._suspendedAt = 0;
        this._sourceNode?.start(0, this.position);
        this._positionRaf.start();

        this.emit('play-state', { target: this, started: this.started, playing: this.playing });

        return this;
    }

    public fadeIn(time?: number): this {
        if (typeof time !== 'undefined') this.fadeTime = time;
        this._fadeTo = 1;
        this._fadeRaf.start();
        return this;
    }

    public fadeOut(time?: number): this {
        if (typeof time !== 'undefined') this.fadeTime = time;
        this._fadeTo = 0;
        this._fadeRaf.start();
        return this;
    }

    public destroy(): void {
        this._positionRaf.destroy();
        this._fadeRaf.destroy();

        this._context = undefined;
        this._fadeNode?.disconnect();
        this._gainNode?.disconnect();
        this._buffer = null;

        this._master = undefined;
        this._channel = undefined;

        this.removeAllListeners();

        this._bus.off('suspend-state', this.busSuspendStateHandler);

        Audio._instances.delete(this.id);
    }

    private setSourceNode(): void {
        this.unsetSourceNode();
        this._sourceNode = this.context.createBufferSource();
        this._sourceNode.buffer = this._buffer;
        this._sourceNode.loop = this._loop;
        this._sourceNode.connect(this.gainNode);
        this._sourceNode.onended = this.audioEndHandler;
    }

    private unsetSourceNode(): void {
        if (!this._sourceNode) return;
        this._sourceNode.stop();
        this._sourceNode.disconnect();
        this._sourceNode.onended = null;
        this._sourceNode = undefined;
    }

    private audioPositionHandler = (): void => {
        this.emit('position', { target: this, position: this.position });
    }

    private audioEndHandler = (): void => {
        this.unsetSourceNode();
        this._positionRaf.stop();
        this._started = false;
        this.emit('position', { target: this, position: this.duration });
        this.emit('play-state', { target: this, started: this.started, playing: this.playing });
    }

    private fadeHandler = (deltaTime: number): void => {
        const multiplier = this._fadeTo === 1 ? 1 : -1;
        let volume = this.fadeNode.gain.value + this._velocity * deltaTime * multiplier;

        volume = Math.max(0, volume);
        volume = Math.min(1, volume);

        this.fadeNode.gain.value = volume;
        this.emit('fade', { target: this, fade: volume, to: this._fadeTo });

        if (volume === this._fadeTo) {
            this._fadeRaf.stop();
        }
    }

    private busSuspendStateHandler: EventListener<BusEventTypes, 'suspend-state'> = ({
        target,
        suspended
    }): void => {
        if (target !== this._master && target !== this._channel) return;
        if (suspended) this.systemSuspended();
        else this.systemResumed();
    }

    private systemSuspended(): void {
        // Not playing? Nothing to do then.
        if (!this.playing) return;

        // Neither master nor channel suspended? Nothing to do then.
        if (this._master?.suspended === false && this._channel?.suspended === false) return;

        this.unsetSourceNode();
        this._positionRaf.stop();

        this._suspendedAt = this.context.currentTime;
    }

    private systemResumed(): void {
        // Not playing? Nothing to do then.
        if (!this.playing) return;

        // Either master or channel suspended? Nothing to do then.
        if (this._master?.suspended || this._channel?.suspended) return;

        this.setSourceNode();
        this._startedAt += this.context.currentTime - this._suspendedAt;
        this._suspendedAt = 0;
        this._sourceNode?.start(0, this.position);
        this._positionRaf.start();
    }
}

export { Audio };
