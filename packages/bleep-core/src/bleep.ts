import { Channel, Master } from './channels';

let master: Master;

function bleep(): Master {
    if (master) return master;

    // Pre-defined master channel with 3 sub-channels.
    // Should be suitable for most use cases.
    master = new Master('master', { volume: 1 });

    // Background channel (music etc.)
    const bg = new Channel('bg', { volume: 0.3 });
    master.addChannel(bg);

    // Effect channel (louder than background channel)
    const fx = new Channel('fx', { volume: 0.7 });
    master.addChannel(fx);

    // Foreground channel (louder than effect channel)
    const fg = new Channel('fg', { volume: 1 });
    master.addChannel(fg);

    return master;
}

export { bleep };
