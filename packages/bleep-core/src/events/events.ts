import { Audio } from '../audio';
import { Channel, Master } from '../channels';

export type EventTarget = Audio | Channel | Master;

export type EventData<
    D extends object = {},
    T extends EventTarget = EventTarget
> = {
    target: T;
} & D;

export type Listener<
    D extends object = {},
    T extends EventTarget = EventTarget
> = {
    (data: EventData<D, T>): void;
};

export type ContextUnlockedListener = Listener<{
    context: AudioContext;
}, Master>;

export type VolumeListener<T extends EventTarget = EventTarget> = Listener<{
    volume: number;
}, T>;

export type MuteStateListener<T extends EventTarget = EventTarget> = Listener<{
    muted: boolean;
}, T>;

export type LoopStateListener = Listener<{
    loop: boolean;
}, Audio>;

export type SuspendStateListener<T extends EventTarget = EventTarget> = Listener<{
    suspended: boolean;
}, T>;

export type PlayStateListener = Listener<{
    started: boolean;
    playing: boolean;
}, Audio>;

export type PositionListener = Listener<{
    position: number;
}, Audio>;

export type FadeListener<T extends EventTarget = EventTarget> = Listener<{
    fade: number;
    to: number;
}, T>;

export type ChannelAddedListener = Listener<{
    channel: Channel;
}, Master>;

export type ChannelRemovedListener = Listener<{
    channel: Channel;
}, Master>;

export type AudioAddedListener = Listener<{
    audio: Audio;
}, Channel>;

export type AudioRemovedListener = Listener<{
    audio: Audio;
}, Channel>;

export interface EventTypes {
    'context-unlocked': ContextUnlockedListener;
    'volume': VolumeListener;
    'fade': FadeListener;
    'mute-state': MuteStateListener;
    'loop-state': LoopStateListener;
    'suspend-state': SuspendStateListener;
    'play-state': PlayStateListener;
    'position': PositionListener;
    'channel-added': ChannelAddedListener;
    'channel-removed': ChannelRemovedListener;
    'audio-added': AudioAddedListener;
    'audio-removed': AudioRemovedListener;
}

// /////////////////////////////////////////////////////////////////
// -----------------------------------------------------------------
//                             MASTER
// -----------------------------------------------------------------
// /////////////////////////////////////////////////////////////////

export type MasterVolumeListener = VolumeListener<Master>;
export type MasterFadeListener = FadeListener<Master>;
export type MasterMuteStateListener = MuteStateListener<Master>;
export type MasterSuspendStateListener = SuspendStateListener<Master>;

export interface MasterEventTypes extends Pick<EventTypes,
    'context-unlocked' |
    'channel-added' |
    'channel-removed'
> {
    'volume': MasterVolumeListener;
    'fade': MasterFadeListener;
    'mute-state': MasterMuteStateListener;
    'suspend-state': MasterSuspendStateListener;
}

// /////////////////////////////////////////////////////////////////
// -----------------------------------------------------------------
//                             CHANNEL
// -----------------------------------------------------------------
// /////////////////////////////////////////////////////////////////

export type ChannelVolumeListener = VolumeListener<Channel>;
export type ChannelFadeListener = FadeListener<Channel>;
export type ChannelMuteStateListener = MuteStateListener<Channel>;
export type ChannelSuspendStateListener = SuspendStateListener<Channel>;

export interface ChannelEventTypes extends Pick<EventTypes,
    'audio-added' |
    'audio-removed'
> {
    'volume': ChannelVolumeListener;
    'fade': ChannelFadeListener;
    'mute-state': ChannelMuteStateListener;
    'suspend-state': ChannelSuspendStateListener;
}

// /////////////////////////////////////////////////////////////////
// -----------------------------------------------------------------
//                              AUDIO
// -----------------------------------------------------------------
// /////////////////////////////////////////////////////////////////

export type AudioVolumeListener = VolumeListener<Audio>;
export type AudioFadeListener = FadeListener<Audio>;
export type AudioMuteStateListener = MuteStateListener<Audio>;
export type AudioSuspendStateListener = SuspendStateListener<Audio>;

export interface AudioEventTypes extends Pick<EventTypes,
    'play-state' |
    'position'
> {
    'volume': AudioVolumeListener;
    'fade': AudioFadeListener;
    'mute-state': AudioMuteStateListener;
    'loop-state': LoopStateListener;
    'suspend-state': AudioSuspendStateListener;
}

// /////////////////////////////////////////////////////////////////
// -----------------------------------------------------------------
//                               BUS
// -----------------------------------------------------------------
// /////////////////////////////////////////////////////////////////

export interface BusEventTypes {
    'suspend-state': SuspendStateListener<Channel | Master>;
}
