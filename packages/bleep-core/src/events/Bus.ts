/* eslint-disable no-underscore-dangle */
import EventEmitter, { EventNames, EventArgs } from 'eventemitter3';

import { BusEventTypes } from './events';

const bus: EventEmitter<BusEventTypes> = new EventEmitter();

type BusEventNames = EventNames<BusEventTypes>;
type BusEventArgs = EventArgs<BusEventTypes, BusEventNames>;

class Bus<Events extends object> extends EventEmitter<Events> {
    protected _bus = bus;

    protected dispatch<T extends EventNames<Events>>(event: T, ...args: EventArgs<Events, T>): void {
        // First emit the event on the internal event bus.
        this._bus.emit(event as BusEventNames, ...args as BusEventArgs);

        // Then emit for the public.
        this.emit(event, ...args);
    }
}

export { Bus };
