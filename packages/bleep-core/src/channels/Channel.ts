/* eslint-disable no-underscore-dangle */
import { Audio } from '../audio';
import { Bus, ChannelEventTypes } from '../events';
import { Raf } from '../utils';

export interface ChannelOptions {
    volume?: number;
    fadeTime?: number;
    fadeTo?: 0 | 1;
}

class Channel extends Bus<ChannelEventTypes> {
    private static _instances: Map<string, Channel> = new Map();

    private _context?: AudioContext;
    private _gainNode?: GainNode;
    private _fadeNode?: GainNode;

    private _id: string;
    private _volume: number;
    private _muted = false;
    private _suspended = false;
    private _audios: Map<string, Audio> = new Map();
    private _fadeRaf: Raf;
    private _fadeTo: 0 | 1;
    private _fadeTime = 2;
    private _velocity = 0.0005;

    constructor(id: string, options?: ChannelOptions) {
        super();

        // Ensure unique identifiers.
        if (Channel._instances.has(id)) {
            throw new Error([
                `Channel with id "${id}" already exists.`,
                'Each channel must be instantiated with a unique id.'
            ].join(' '));
        }

        Channel._instances.set(id, this);

        const {
            volume = 1,
            fadeTime = 2,
            fadeTo = 1
        } = options || {};

        this._id = id;
        this._volume = volume;
        this.fadeTime = fadeTime;
        this._fadeTo = fadeTo;

        this._fadeRaf = new Raf(Infinity, this.fadeHandler);
    }

    public static getChannel(channelId: string): Channel | undefined {
        return Array.from(Channel._instances.values()).find(
            (channel) => channel.id === channelId
        );
    }

    public static getChannelOfAudio(audioId: string): Channel | undefined {
        return Array.from(Channel._instances.values()).find(
            (i) => i.hasAudio(audioId)
        );
    }

    // internal (usually)
    public connect(node: AudioNode, context: AudioContext): this {
        if (this._context) {
            /* eslint-disable-next-line no-console */
            console.warn([
                '[@heinzelman-labs/bleep-core]',
                `Channel with id "${this.id}" already connected.`
            ].join(' '));
            return this;
        }

        this._context = context;

        this.gainNode.connect(this.fadeNode);
        this.gainNode.gain.value = this._muted ? 0 : this._volume;
        this.fadeNode.connect(node);

        return this;
    }

    // internal (usually)
    public disconnect(): this {
        if (!this._context) {
            /* eslint-disable-next-line no-console */
            console.warn([
                '[@heinzelman-labs/bleep-core]',
                `Channel with id "${this.id}" already disconnected.`
            ].join(' '));
            return this;
        }

        this._context = undefined;

        this.fadeNode.disconnect();
        this.gainNode.disconnect();
        this.gainNode.gain.value = this._muted ? 0 : this._volume;

        return this;
    }

    private get context(): AudioContext {
        if (!this._context) {
            throw new Error([
                '[@heinzelman-labs/bleep-core]',
                `Channel with id "${this.id}" is not connected (context not set).`
            ].join(' '));
        }
        return this._context;
    }

    private get gainNode(): GainNode {
        if (!this._gainNode) {
            this._gainNode = this.context.createGain();
        }
        return this._gainNode;
    }

    private get fadeNode(): GainNode {
        if (!this._fadeNode) {
            this._fadeNode = this.context.createGain();
        }
        return this._fadeNode;
    }

    get id(): string {
        return this._id;
    }

    get fading(): boolean {
        return this._fadeRaf.started;
    }

    get fadingIn(): boolean {
        return this.fading && this._fadeTo === 1;
    }

    get fadingOut(): boolean {
        return this.fading && this._fadeTo === 0;
    }

    get fade(): number {
        return this.fadeNode.gain.value;
    }

    get fadeTo(): number {
        return this._fadeTo;
    }

    get fadeTime(): number {
        return this._fadeTime;
    }

    // sec
    set fadeTime(value: number) {
        this._fadeTime = value;
        this._velocity = 1 / (value * 1000);
    }

    get muted(): boolean {
        return this._muted;
    }

    set muted(value: boolean) {
        if (value === this._muted) return;
        this._muted = value;
        this.gainNode.gain.value = value ? 0 : this.volume;
        this.emit('mute-state', { target: this, muted: value });
    }

    get volume(): number {
        return this._volume;
    }

    set volume(value: number) {
        this._volume = value;
        if (!this.muted) this.gainNode.gain.value = value;
        this.emit('volume', { target: this, volume: value });
    }

    get suspended(): boolean {
        return this._suspended;
    }

    public getAudios(): Audio[] {
        return Array.from(this._audios.values());
    }

    public hasAudio(id: string): boolean {
        return this._audios.has(id);
    }

    public getAudio(id: string): Audio | undefined {
        return this._audios.get(id);
    }

    public addAudio(audio: Audio): this {
        if (this.hasAudio(audio.id)) {
            /* eslint-disable-next-line no-console */
            console.warn([
                '[@heinzelman-labs/bleep-core]',
                `Audio with id "${audio.id}" already added to channel "${this.id}".`
            ].join(' '));
            return this;
        }

        // Important to add the channel before connecting it.
        this._audios.set(audio.id, audio);
        audio.connect(this.gainNode, this.context);

        this.emit('audio-added', { target: this, audio });
        return this;
    }

    public removeAudio(id: string, destroy = false): this {
        const audio = this.getAudio(id);
        if (audio) {
            audio.disconnect();
            if (destroy) audio.destroy();
            this._audios.delete(id);
            this.emit('audio-removed', { target: this, audio });
        }
        return this;
    }

    public removeAudios(destroy = false): this {
        this._audios.forEach(
            (audio) => this.removeAudio(audio.id, destroy)
        );
        return this;
    }

    public purgeAudio(id: string): this {
        this.removeAudio(id, true);
        return this;
    }

    public purgeAudios(): this {
        this.removeAudios(true);
        return this;
    }

    public suspend(): this {
        if (this._suspended) return this;
        this._suspended = true;
        this.dispatch('suspend-state', { target: this, suspended: true });
        return this;
    }

    public resume(): this {
        if (!this._suspended) return this;
        this._suspended = false;
        this.dispatch('suspend-state', { target: this, suspended: false });
        return this;
    }

    public fadeIn(time?: number): this {
        if (typeof time !== 'undefined') this.fadeTime = time;
        this._fadeTo = 1;
        this._fadeRaf.start();
        return this;
    }

    public fadeOut(time?: number): this {
        if (typeof time !== 'undefined') this.fadeTime = time;
        this._fadeTo = 0;
        this._fadeRaf.start();
        return this;
    }

    // internal unless standalone usage (if that will be possible)
    public destroy(): void {
        this.purgeAudios();

        this._fadeRaf.destroy();
        this._fadeNode?.disconnect();
        this._gainNode?.disconnect();

        this.removeAllListeners();

        Channel._instances.delete(this.id);
    }

    private fadeHandler = (deltaTime: number): void => {
        const multiplier = this._fadeTo === 1 ? 1 : -1;
        let volume = this.fadeNode.gain.value + this._velocity * deltaTime * multiplier;

        volume = Math.max(0, volume);
        volume = Math.min(1, volume);

        this.fadeNode.gain.value = volume;
        this.emit('fade', { target: this, fade: volume, to: this._fadeTo });

        if (volume === this._fadeTo) {
            this._fadeRaf.stop();
        }
    }
}

export { Channel };
