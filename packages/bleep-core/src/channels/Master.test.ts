/* eslint-disable no-new */
import { Master } from './Master';

describe('Master', () => {
    it('throws error on duplicated identifiers', () => {
        expect(() => { new Master('master'); }).not.toThrow();
        expect(() => { new Master('master'); }).toThrow();
    });
});
