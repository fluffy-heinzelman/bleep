import { Channel, Master } from '.';

export const isMaster = (value: any): value is Master => {
    return value instanceof Master;
};

export const isChannel = (value: any): value is Channel => {
    return value instanceof Channel;
};
