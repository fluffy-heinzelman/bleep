/* eslint-disable no-underscore-dangle */
import { Bus, MasterEventTypes } from '../events';
import { Raf, unlockAudioContext, webAudioContext, webAudioSupport } from '../utils';
import { Channel } from './Channel';

export interface MasterOptions {
    volume?: number;
    fadeTime?: number;
    fadeTo?: 0 | 1;
    context?: AudioContext;
    handleTabChange?: boolean;
}

class Master extends Bus<MasterEventTypes> {
    private static _instances: Map<string, Master> = new Map();

    private _context?: AudioContext;
    private _gainNode?: GainNode;
    private _fadeNode?: GainNode;

    private _id: string;
    private _volume = 1;
    private _muted = false;
    private _suspended = false;
    private _channels: Map<string, Channel> = new Map();
    private _fadeRaf: Raf;
    private _fadeTo: 0 | 1 = 1;
    private _fadeTime = 2;
    private _velocity = 0.0005;

    constructor(id: string, options?: MasterOptions) {
        super();

        // Ensure unique Master identifiers.
        if (Master._instances.has(id)) {
            throw new Error([
                '[@heinzelman-labs/bleep-core]',
                `Master with id "${id}" already exists.`,
                'Each master must be instantiated with a unique id.'
            ].join(' '));
        }

        Master._instances.set(id, this);

        const {
            volume = 1,
            fadeTime = 2,
            fadeTo = 1,
            context,
            handleTabChange = true
        } = options || {};

        this._id = id;
        this._context = context;
        this.fadeTime = fadeTime;
        this._fadeTo = fadeTo;
        this._fadeRaf = new Raf(Infinity, this.fadeHandler);

        this.gainNode.connect(this.fadeNode);
        this.fadeNode.connect(this.context.destination);
        this.volume = volume;

        unlockAudioContext(this.context);

        if (handleTabChange) {
            document.addEventListener('visibilitychange', this.tabChangeHandler);
            document.addEventListener('pagehide', this.tabChangeHandler);
        }
    }

    public static getMaster(masterId: string): Master | undefined {
        return Array.from(Master._instances.values()).find(
            (master) => master.id === masterId
        );
    }

    public static getMasterOfChannel(channelId: string): Master | undefined {
        return Array.from(Master._instances.values()).find(
            (i) => i.hasChannel(channelId)
        );
    }

    public static getMasterOfAudio(audioId: string): Master | undefined {
        return Array.from(Master._instances.values()).find((master) => {
            return master.getChannels().some(
                (channel) => channel.hasAudio(audioId)
            );
        });
    }

    get context(): AudioContext {
        if (!this._context) {
            if (!webAudioContext || !webAudioSupport) {
                throw new Error('[@heinzelman-labs/bleep-core] WebAudio not supported');
            }
            this._context = webAudioContext;
        }
        return this._context;
    }

    private get gainNode(): GainNode {
        if (!this._gainNode) {
            this._gainNode = this.context.createGain();
        }
        return this._gainNode;
    }

    private get fadeNode(): GainNode {
        if (!this._fadeNode) {
            this._fadeNode = this.context.createGain();
        }
        return this._fadeNode;
    }

    get id(): string {
        return this._id;
    }

    get fading(): boolean {
        return this._fadeRaf.started;
    }

    get fadingIn(): boolean {
        return this.fading && this._fadeTo === 1;
    }

    get fadingOut(): boolean {
        return this.fading && this._fadeTo === 0;
    }

    get fade(): number {
        return this.fadeNode.gain.value;
    }

    get fadeTo(): number {
        return this._fadeTo;
    }

    get fadeTime(): number {
        return this._fadeTime;
    }

    // sec
    set fadeTime(value: number) {
        this._fadeTime = value;
        this._velocity = 1 / (value * 1000);
    }

    get muted(): boolean {
        return this._muted;
    }

    set muted(value: boolean) {
        if (value === this._muted) return;
        this._muted = value;
        this.gainNode.gain.value = value ? 0 : this._volume;
        this.emit('mute-state', { target: this, muted: value });
    }

    get volume(): number {
        return this._volume;
    }

    set volume(value: number) {
        this._volume = value;
        if (!this.muted) this.gainNode.gain.value = value;
        this.emit('volume', { target: this, volume: value });
    }

    get suspended(): boolean {
        return this._suspended;
    }

    public getChannels(): Channel[] {
        return Array.from(this._channels.values());
    }

    public hasChannel(id: string): boolean {
        return this._channels.has(id);
    }

    public getChannel(id: string): Channel | undefined {
        return this._channels.get(id);
    }

    public addChannel(channel: Channel): this {
        if (this.hasChannel(channel.id)) {
            /* eslint-disable-next-line no-console */
            console.warn([
                '[@heinzelman-labs/bleep-core]',
                `Channel with id "${channel.id}" already added to master "${this.id}".`
            ].join(' '));
            return this;
        }

        // Important to add the channel before connecting it.
        this._channels.set(channel.id, channel);
        channel.connect(this.gainNode, this.context);

        this.emit('channel-added', { target: this, channel });
        return this;
    }

    public removeChannel(id: string, destroy = false): this {
        const channel = this.getChannel(id);
        if (channel) {
            channel.disconnect();
            if (destroy) channel.destroy();
            this._channels.delete(id);
            this.emit('channel-removed', { target: this, channel });
        }
        return this;
    }

    public removeChannels(destroy = false): this {
        this._channels.forEach(
            (channel) => this.removeChannel(channel.id, destroy)
        );
        return this;
    }

    public purgeChannel(id: string): this {
        this.removeChannel(id, true);
        return this;
    }

    public purgeChannels(): this {
        this.removeChannels(true);
        return this;
    }

    public suspend(): this {
        if (this._suspended) return this;
        this._suspended = true;
        this.dispatch('suspend-state', { target: this, suspended: true });
        return this;
    }

    public resume(): this {
        if (!this._suspended) return this;
        this._suspended = false;
        this.dispatch('suspend-state', { target: this, suspended: false });
        return this;
    }

    public fadeIn(time?: number): this {
        if (typeof time !== 'undefined') this.fadeTime = time;
        this._fadeTo = 1;
        this._fadeRaf.start();
        return this;
    }

    public fadeOut(time?: number): this {
        if (typeof time !== 'undefined') this.fadeTime = time;
        this._fadeTo = 0;
        this._fadeRaf.start();
        return this;
    }

    public destroy(): void {
        this.purgeChannels();

        this._fadeRaf.destroy();
        this._fadeNode?.disconnect();
        this._gainNode?.disconnect();

        this.removeAllListeners();
        document.removeEventListener('visibilitychange', this.tabChangeHandler);
        document.removeEventListener('pagehide', this.tabChangeHandler);

        Master._instances.delete(this.id);
    }

    private fadeHandler = (deltaTime: number): void => {
        const multiplier = this._fadeTo === 1 ? 1 : -1;
        let volume = this.fadeNode.gain.value + this._velocity * deltaTime * multiplier;

        volume = Math.max(0, volume);
        volume = Math.min(1, volume);

        this.fadeNode.gain.value = volume;
        this.emit('fade', { target: this, fade: volume, to: this._fadeTo });

        if (volume === this._fadeTo) {
            this._fadeRaf.stop();
        }
    }

    private tabChangeHandler = (): void => {
        if (document.hidden) this._context?.suspend();
        else this._context?.resume();
    }
}

export { Master };
