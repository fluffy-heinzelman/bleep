/* eslint-disable no-new */
import { Channel } from './Channel';

describe('Channel', () => {
    it('throws error on duplicated identifiers', () => {
        expect(() => { new Channel('channel'); }).not.toThrow();
        expect(() => { new Channel('channel'); }).toThrow();
    });
});
