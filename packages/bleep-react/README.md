# Bleep - React library

*This is still work-in-progress!*

Everything you need to use `Bleep` in React based projects.

## Demo

Have fun playing with the [Demo](https://fluffy-heinzelman.gitlab.io/bleep/).

## Credits

The development of Bleep would have been half the fun without the hard work of some great music artists and game enthusiasts. Special thanks therefore go to, among others:

* [FesliyanStudios](https://www.fesliyanstudios.com)
* [sawsquarenoise](http://rolemusic.sawsquarenoise.com/p/sawsquarenoise.html)
* [OpenGameArt](https://opengameart.org/)

## License

Bleep software is [MIT licensed](./LICENSE.md).
