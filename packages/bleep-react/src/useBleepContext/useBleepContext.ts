import { useContext } from 'react';

import { BleepContext, BleepContextValue } from '../BleepProvider';

const useBleepContext = (): BleepContextValue => useContext<BleepContextValue>(BleepContext);

export { useBleepContext };
