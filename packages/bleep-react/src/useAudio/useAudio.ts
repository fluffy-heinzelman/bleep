import { Audio, Channel, isAudio, Master } from '@heinzelman-labs/bleep-core';

import { OrUndef } from '../types';

function useAudio(audio: Audio): [Audio, OrUndef<Channel>, OrUndef<Master>];
function useAudio(audio: string): [OrUndef<Audio>, OrUndef<Channel>, OrUndef<Master>];
function useAudio(audio: string | Audio): [OrUndef<Audio>, OrUndef<Channel>, OrUndef<Master>];

function useAudio(audio: string | Audio): any {
    let m: Master | undefined;
    let c: Channel | undefined;
    let a: Audio | undefined;

    // Use case 1: An audio instance was passed.
    // We try to complement the data with a master and
    // channel instance.
    if (isAudio(audio)) {
        m = Master.getMasterOfAudio(audio.id);
        c = Channel.getChannelOfAudio(audio.id);
        a = audio;
    }

    // Use case 2: An audio identifier was passed.
    // We try to complement the data with a master and
    // channel instance. If we find a channel, we can
    // retrieve the audio from it.
    else if (audio) {
        m = Master.getMasterOfAudio(audio);
        c = Channel.getChannelOfAudio(audio);
        a = c?.getAudio(audio);
    }

    return [a, c, m];
}

export { useAudio };
