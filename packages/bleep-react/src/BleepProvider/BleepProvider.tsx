import React, { FunctionComponent, memo, ReactElement, ReactNode } from 'react';

import { BleepContext, BleepContextValue } from './context';

export interface BleepProviderProps extends Partial<BleepContextValue> {
    /**
     * @ignore
     */
    children?: ReactNode;
}

const BleepProvider: FunctionComponent<BleepProviderProps> = memo(({
    master: innerMaster,
    channel: innerChannel,
    children
}: BleepProviderProps): ReactElement => {
    return (
        <BleepContext.Consumer>
            {(outerValue): ReactElement => {
                const { master, channel } = outerValue || {};
                const value: BleepContextValue = {
                    master: innerMaster || master,
                    channel: innerChannel || channel
                };

                return (
                    <BleepContext.Provider value={value}>
                        {children}
                    </BleepContext.Provider>
                );
            }}
        </BleepContext.Consumer>
    );
});

BleepProvider.displayName = 'BleepProvider';

export { BleepProvider };
