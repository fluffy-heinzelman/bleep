import React from 'react';
import { Channel, Master } from '@heinzelman-labs/bleep-core';

export interface BleepContextValue {
    /**
     * Audio master channel.
     */
    master?: Master;

    /**
     * Audio channel.
     * @internal
     * @ignore
     */
    channel?: Channel;
}

export const BleepContext = React.createContext<BleepContextValue>({});
