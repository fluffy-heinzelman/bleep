import { useRef } from 'react';

const uninitialized = Symbol('useInstance uninitialized ref');

const useInstance = <T extends new (...args: any) => any>(
    Ctor: T,
    ...rest: ConstructorParameters<T>
): InstanceType<T> => {
    const ref = useRef<InstanceType<T>>(uninitialized as any);

    if (ref.current === uninitialized) {
        ref.current = new Ctor(...rest);
    }

    return ref.current;
};

export { useInstance };
