import { useEffect } from 'react';
import {
    Audio,
    AudioEventTypes,
    Channel,
    ChannelEventTypes,
    EventEmitter,
    EventTarget,
    EventTypes,
    Master,
    MasterEventTypes
} from '@heinzelman-labs/bleep-core';

function useEvent<E extends EventEmitter.EventNames<MasterEventTypes>>(
    target: Master | undefined,
    event: E,
    listener: EventEmitter.EventListener<MasterEventTypes, E>,
    context?: any
): void;

function useEvent<E extends EventEmitter.EventNames<ChannelEventTypes>>(
    target: Channel | undefined,
    event: E,
    listener: EventEmitter.EventListener<ChannelEventTypes, E>,
    context?: any
): void;

function useEvent<E extends EventEmitter.EventNames<AudioEventTypes>>(
    target: Audio | undefined,
    event: E,
    listener: EventEmitter.EventListener<AudioEventTypes, E>,
    context?: any
): void;

function useEvent<E extends EventEmitter.EventNames<EventTypes>>(
    target: EventTarget | undefined,
    event: E,
    listener: EventEmitter.EventListener<EventTypes, E>,
    context?: any
): void;

function useEvent(target: any, event: any, listener: any, context?: any): void {
    useEffect(() => {
        target?.on(event, listener, context);
        return () => {
            target?.off(event, listener, context);
        };
    }, [
        target,
        event,
        listener,
        context
    ]);
}

export { useEvent };
