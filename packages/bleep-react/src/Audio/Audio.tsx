import React, { Fragment, ReactNode, useEffect } from 'react';
import { Audio as CoreAudio, AudioOptions } from '@heinzelman-labs/bleep-core';

import { useBleepContext } from '../useBleepContext';
import { useChannel } from '../useChannel';
import { useInstance } from '../useInstance';

export interface AudioProps extends AudioOptions {
    /**
     * Audio id.
     */
    id: string;

    /**
     * Channel id. Only necessary if not nested in a `<Channel>`.
     * element.
     */
    channelId?: string;

    /**
     * Audio source as `AudioBuffer`.
     */
    src: AudioBuffer;

    /**
     * @ignore
     */
    children?: ReactNode;
}

const Audio = ({
    id,
    channelId,
    src,
    children,
    ...options
}: AudioProps): JSX.Element => {
    const audio = useInstance<typeof CoreAudio>(CoreAudio, id, src, options);
    const ctx = useBleepContext();
    const cId = channelId || ctx.channel?.id || '';
    const [channel] = useChannel(cId);

    if (channel && !channel.hasAudio(id)) {
        channel.addAudio(audio);
    }

    // Destroy when unmounted.
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
    useEffect(() => audio.destroy.bind(audio), []);

    return (
        <Fragment>
            {children}
        </Fragment>
    );
};

Audio.displayName = 'Audio';

export { Audio };
