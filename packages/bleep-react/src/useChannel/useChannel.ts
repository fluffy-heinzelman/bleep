import { Channel, Master, isChannel } from '@heinzelman-labs/bleep-core';

import { useBleepContext } from '../useBleepContext';
import { OrUndef } from '../types';

function useChannel(channel: Channel): [Channel, OrUndef<Master>];
function useChannel(channel: string): [OrUndef<Channel>, OrUndef<Master>];
function useChannel(): [OrUndef<Channel>, OrUndef<Master>];
function useChannel(channel?: string | Channel): [OrUndef<Channel>, OrUndef<Master>];

function useChannel(channel?: string | Channel): any {
    const ctx = useBleepContext();

    let m: Master | undefined;
    let c: Channel | undefined;

    // Use case 1: A channel instance was passed.
    // We try to complement the data with a master instance.
    if (isChannel(channel)) {
        m = Master.getMasterOfChannel(channel.id);
        c = channel;
    }

    // Use case 2: A channel identifier was passed.
    // We try to complement the data with a master instance.
    // If we find one, we can retrieve the channel from it.
    else if (channel) {
        m = Master.getMasterOfChannel(channel);
        c = m?.getChannel(channel);
    }

    // Use case 3: Nothing was passed.
    // Master and channel are retrieved from context.
    else {
        ({ master: m, channel: c } = ctx);
    }

    return [c, m];
}

export { useChannel };
