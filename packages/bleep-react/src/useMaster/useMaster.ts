import { Master, isMaster } from '@heinzelman-labs/bleep-core';

import { useBleepContext } from '../useBleepContext';
import { OrUndef } from '../types';

function useMaster(master: Master): Master;
function useMaster(master: string): OrUndef<Master>;
function useMaster(): OrUndef<Master>;
function useMaster(master?: string | Master): OrUndef<Master>;

function useMaster(master?: string | Master): any {
    const ctx = useBleepContext();

    let m: Master | undefined;

    // Use case 1: A master instance was passed.
    if (isMaster(master)) {
        m = master;
    }

    // Use case 2: A master identifier was passed.
    else if (master) {
        m = Master.getMaster(master);
    }

    // Use case 3: Nothing was passed.
    // Master is retrieved from context.
    else {
        ({ master: m } = ctx);
    }

    return m;
}

export { useMaster };
