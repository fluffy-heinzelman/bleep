import React, { ReactNode, useEffect } from 'react';
import { Master as CoreMaster, MasterOptions } from '@heinzelman-labs/bleep-core';

import { BleepProvider } from '../BleepProvider';
import { useInstance } from '../useInstance';

export interface MasterProps extends MasterOptions {
    /**
     * Master id.
     */
    id: string;

    /**
     * @ignore
     */
    children?: ReactNode;
}

const Master = ({
    id,
    children,
    ...options
}: MasterProps): JSX.Element => {
    const master = useInstance<typeof CoreMaster>(CoreMaster, id, options);

    // Destroy when unmounted.
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
    useEffect(() => master.destroy.bind(master), []);

    return (
        <BleepProvider master={master}>
            {children}
        </BleepProvider>
    );
};

Master.displayName = 'Master';

export { Master };
