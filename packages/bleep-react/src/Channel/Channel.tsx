import React, { ReactNode, useEffect } from 'react';
import { Channel as CoreChannel, ChannelOptions } from '@heinzelman-labs/bleep-core';

import { BleepProvider } from '../BleepProvider';
import { useInstance } from '../useInstance';
import { useMaster } from '../useMaster';

export interface ChannelProps extends ChannelOptions {
    /**
     * Channel id.
     */
    id: string;

    /**
     * @ignore
     */
    children?: ReactNode;
}

const Channel = ({
    id,
    children,
    ...options
}: ChannelProps): JSX.Element => {
    const channel = useInstance<typeof CoreChannel>(CoreChannel, id, options);
    const master = useMaster();

    if (master && !master.hasChannel(id)) {
        master.addChannel(channel);
    }

    // Destroy when unmounted.
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
    useEffect(() => channel.destroy.bind(channel), []);

    return (
        <BleepProvider channel={channel}>
            {children}
        </BleepProvider>
    );
};

Channel.displayName = 'Channel';

export { Channel };
